﻿{%USUARIO%}:
                
Se reporta el siguiente Incidente:

Fecha y Hora: {%CCREATEDDATE%}
Tipo Incidente: {%NAMEINCIDENCETYPE%}
Sub Tipo Incidente: {%NAMEINCIDENCESUBTYPE%}
Lugar: {%PLACE%}
Descripcion: <b>{%DESCRIPTION%}
Reporta: {%NAMELASTNAME%}
Cliente: {%CUSTOMERNAME%}

No responda a este buzón de correo.

