﻿using BAZ.GEN.Log4Dan;
using EVP.RIS.Data;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web;
using System.Web.Http.Filters;

namespace EVP.RIS.APIService.Filters
{
    public class AuthTokenFilter : ActionFilterAttribute
    {
        private Log Log = new Log(typeof(AuthTokenFilter));

        public DLAppMovil dlAppMovil;

        public AuthTokenFilter()
        {
            dlAppMovil = new DLAppMovil();
        }

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            
            // Name of meta data to appear in header of each request
            const string secretTokenName = "Auth-Token";

            var goodRequest = false;
            var errorResponse = new HttpResponseMessage(HttpStatusCode.Unauthorized);

            // The request should have the secretTokenName in the header containing the shared secret
            if (actionContext.Request.Headers.Contains(secretTokenName))
            {
                var messageSecretValue = actionContext.Request.Headers.GetValues(secretTokenName).First();

                int codeResult = dlAppMovil.doValidateTokenSesion(messageSecretValue);

                if (codeResult == 0)
                {
                    goodRequest = true;
                }
            }
            else
            {
                errorResponse = new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            if (!goodRequest)
            {
                Thread.Sleep(2000);

                actionContext.Response = errorResponse;
            }


            base.OnActionExecuting(actionContext);
        }
    }
}