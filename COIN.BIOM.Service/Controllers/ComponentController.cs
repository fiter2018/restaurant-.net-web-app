﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BAZ.GEN.Log4Dan;
using COIN.BIOM.Data;
using COIN.BIOM.Entity;
using System.Configuration;
using COIN.BIOM.Entity.model;
using System.Globalization;
using COIN.BIOM.Entity.query;

namespace COIN.BIOM.Service.Controllers
{
    [RoutePrefix("api/component")]
    public class ComponentController : ApiController
    {

        private DLComponent oDLComponent;
        private DLUser oDLUser;
        private DLUserProcess oDLUserProcess;
        private DLProcess oDLProcess;
        private DLEnroll oDLEnroll;
        private DLEventLog oDLEventLog;
        private Log Log = new Log(typeof(EnrollController));

        public ComponentController()
        {
            oDLComponent = new DLComponent();
            oDLUser = new DLUser();
            oDLUserProcess = new DLUserProcess();
            oDLProcess = new DLProcess();
            oDLEnroll = new DLEnroll();
            oDLEventLog = new DLEventLog();
        }


        [HttpGet]
        [Route("getUserEnroll")]
        public IHttpActionResult getUserEnroll(string userBussinessCode, string tokenCode)
        {
            try
            {
                UserEnrollQuery userEnroll = null;
                string tockenIn = tokenCode;
                if (tockenIn == ConfigurationManager.AppSettings["COMPONENT_TOKEN"])
                {
                    User user = oDLUser.GetUserByUserBussinessCode(userBussinessCode);

                    userEnroll = new UserEnrollQuery();
                    userEnroll.userCode = user.userCode;
                    userEnroll.name = user.name;
                    userEnroll.lastName = user.lastName;
                    userEnroll.dni = Convert.ToString(user.dni);
                    userEnroll.fingersTemplate = oDLEnroll.GetEnrollByUser(user.userId);
                }

                return Ok(userEnroll);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                throw ex;
            }
        }

        [HttpGet]
        [Route("getUserEnrollsByProfile")]
        public IHttpActionResult getUserEnrollsByProfile(string tokenCode, int profileId)
        {
            try
            {

                List<UserEnrollQuery> userEnrolls = null;
                string tockenIn = tokenCode;
                if (tockenIn == ConfigurationManager.AppSettings["COMPONENT_TOKEN"])
                {
                    List<UserEnrollQuery> users = oDLEnroll.GetUserWithEnrollByProfile(profileId);
                    List<EnrollQuery> enrolls = oDLEnroll.GetUserEnrollByProfile(profileId);

                    userEnrolls = (from i in users
                                   select new UserEnrollQuery
                                   {
                                       userCode = i.userCode,
                                       name = i.name,
                                       lastName = i.lastName,
                                       dni = i.dni,
                                       fingersTemplate = enrolls.Where(p => p.userCode == i.userCode).ToList()
                                   }).ToList();

                }

                return Ok(userEnrolls);

            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                throw ex;
            }
        }

        [HttpGet]
        [Route("getUserEnrollsByProfile")]
        public IHttpActionResult getUserEnrollsByProcess(string tokenCode, string processBussinessCode)
        {
            try
            {

                List<UserEnrollQuery> userEnrolls = null;
                string tockenIn = tokenCode;
                if (tockenIn == ConfigurationManager.AppSettings["COMPONENT_TOKEN"])
                {/*
                    List<UserEnrollQuery> users = oDLEnroll.GetUserWithEnrollByProfile(profileId);
                    List<EnrollQuery> enrolls = oDLEnroll.GetUserEnrollByProfile(profileId);
                    
                    userEnrolls = (from i in users
                                   select new UserEnrollQuery
                                   {
                                       userCode = i.userCode,
                                       name = i.name,
                                       lastName = i.lastName,
                                       dni = i.dni,
                                       fingersTemplate = enrolls.Where(p => p.userCode == i.userCode).ToList()
                                   }).ToList();
                                   */
                }

                return Ok(userEnrolls);

            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                throw ex;
            }
        }


        [HttpPost]
        [Route("saveFingertemplate")]
        public IHttpActionResult saveFingertemplate(EnrollModel model)
        {
            try
            {
                bool resul;
                string tockenIn = model.tokenCode;

                if (tockenIn == ConfigurationManager.AppSettings["COMPONENT_TOKEN"])
                {
                    User user = oDLUser.Get(model.userCode);

                    Enroll enroll = new Enroll();
                    enroll.userId = user.userId;
                    enroll.FingerTemplate = model.fingerTemplate;

                    resul = oDLComponent.Save(enroll);
                }
                else
                {
                    resul = false;
                }

                return Ok(resul);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                throw ex;
            }
        }

        [HttpPost]
        [Route("saveCompleteUser")]
        public IHttpActionResult saveCompleteUser(EnrollUserModel model)
        {
            try
            {
                bool resul = false;
                string message = "";

                int validationCode = 0;
                string tockenIn = model.tokenCode;

                if (tockenIn == ConfigurationManager.AppSettings["COMPONENT_TOKEN"])
                {
                    //Validar el usuario primero
                    User user = new User();
                    user.userBussinessCode = model.userBussinessCode;

                    Process process = new Process();
                    process.processBussinessCode = model.processBussinessCode;

                    validationCode = oDLComponent.doValidatePreRegisterUser(user,process);

                    if (validationCode == 2)
                    {
                        resul = false;
                        message = "Ya existe un usuario no activo registrado con código ingresado.";
                        return Ok(new { resul = resul, message = message });
                    }

                    if (validationCode == 3)
                    {
                        resul = false;
                        message = "Ya existe un usuario activo registrado con código ingresado.";
                        return Ok(new { resul = resul, message = message });
                    }

                    if (validationCode == 4)
                    {
                        resul = false;
                        message = "Ya existe un usuario activo y asignado al proceso, registrado con código ingresado.";
                        return Ok(new { resul = resul, message = message });
                    }


                    if (validationCode == 1)
                    {
                        user.name = model.name;
                        user.lastName = model.lastName;
                        user.dni = Convert.ToInt32(model.dni);
                        user.userName = user.userBussinessCode;
                        user.password = user.userBussinessCode;
                        user.email = user.userBussinessCode;
                        user.profileId = 3;
                        user.createdUser = 1;
                        user.userCode = null;

                        bool resultSaveUser = oDLUser.CreateUser(ref user);
                        User userWithId = oDLUser.GetUserByUserBussinessCode(user.userBussinessCode);
                        user.userId = userWithId.userId;

                        Process processWithId = oDLProcess.GetByProcessBussinessCode(process.processBussinessCode);
                        process.processId = processWithId.processId;

                        UserProcess userProcess = new UserProcess();
                        userProcess.userId = user.userId;
                        userProcess.processId = process.processId;

                        bool resulAssignUserToProcess = oDLUserProcess.IncludeUserToProcess(ref userProcess);                        

                        Enroll enroll = new Enroll();
                        enroll.userId = user.userId;
                        enroll.FingerTemplate = model.fingerTemplate;

                        bool resulSaveEnroll = oDLEnroll.Save(enroll);

                        resul = true;
                        message = "Usuario registrado satisfactoriamente.";

                    }

                    //resul = oDLComponent.Save(model);
                }
                else
                {
                    resul = false;
                    message = "El token enviado no es válido.";
                    return Ok(new { resul = resul, message = message });
                }

                //Voy a necesitar algun mensaje de validación
                return Ok(new { resul = resul, message = message });
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                return Ok(new { resul = false, message = ex.Message });
            }
        }



        [HttpPost]
        [Route("saveEventLog")]
        public IHttpActionResult saveEventLog(EventLogModel model)
        {
            try
            {
                bool resul = false;
                string message = string.Empty;
                string tockenIn = model.tokenCode;

                if (tockenIn == ConfigurationManager.AppSettings["COMPONENT_TOKEN"])
                {
                    Process proccess = oDLProcess.GetByProcessBussinessCode(model.processBussinessCode);
                    User user = oDLUser.GetUserByUserBussinessCode(model.userBussinessCode);

                    EventLog eventLog = new EventLog();
                    eventLog.processId = proccess.processId;
                    eventLog.userId = user.userId;
                    eventLog.eventResult = model.eventResult;
                    eventLog.eventDatetime = DateTime.ParseExact(model.eventDateTime, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                    bool resulSaveEventLog = oDLEventLog.Save(eventLog);

                    resul = true;
                    message = "Evento registrado satisfactoriamente.";
                }
                else
                {
                    resul = false;
                    message = "El token enviado no es válido.";
                }

                //Voy a necesitar algun mensaje de validación
                return Ok(new { resul = resul, message = message });
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                return Ok(new { resul = false, message = ex.Message });
            }
        }











    }
}
