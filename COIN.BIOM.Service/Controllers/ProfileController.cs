﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using COIN.BIOM.Entity;
using COIN.BIOM.Data;

namespace COIN.BIOM.Service.Controllers
{
    [RoutePrefix("api/profile")]
    public class ProfileController : ApiController
    {
        private DLProfile dlProfile;

        public ProfileController()
        {
            dlProfile = new DLProfile();
        }

        [HttpGet]
        [Route("getProfile")]
        public IHttpActionResult GetProfile()
        {
            List<Profile> resul = dlProfile.GetProfile();

            return Ok(resul);
        }
    }
}
