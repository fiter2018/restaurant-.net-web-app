﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.query;
using COIN.BIOM.Data;
using BAZ.GEN.Log4Dan;

namespace COIN.BIOM.Service.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        private DLUser dlUser;
        private Log Log = new Log(typeof(UserController));

        public UserController()
        {
            dlUser = new DLUser();            
        }

        [HttpGet]
        [Route("getAllUsers")]
        public IHttpActionResult GetAllUsers()
        {
            List<User> resul = dlUser.GetAllUsers();

            return Ok(resul);
        }

        [HttpGet]
        [Route("getUserByParams")]
        public IHttpActionResult getUserByParams(string CodUsuario, string NombUsuario, int Dni, int Perfil)
        {
            List<UserQuery> resul = dlUser.getUserByParams(string.IsNullOrEmpty(CodUsuario) ? string.Empty : CodUsuario, string.IsNullOrEmpty(NombUsuario) ? string.Empty : NombUsuario, Dni, Perfil);

            return Ok(resul);
        }
        /*
        [HttpGet]
        [Route("getUserByProfileCustomer")]
        public IHttpActionResult getUserByProfileCustomer(int customerID = 0, string profileIds = "")
        {
            List<UserQuery> resul = dlUser.getUserByProfileCustomer(customerID, string.IsNullOrEmpty(profileIds) ? string.Empty : profileIds);

            return Ok(resul);
        }
        */

        [HttpGet]
        [Route("get/{id}")]
        public IHttpActionResult Get(string id)
        {
            User item = dlUser.Get(id);
            return Ok(item);
        }

        [HttpGet]
        [Route("GetUserByName")]
        public IHttpActionResult GetUserByName(string userName)
        {
            User item = dlUser.GetUserByName(userName);
            return Ok(item);
        }

        [HttpGet]
        [Route("GetUserByUserBussinessCode")]
        public IHttpActionResult GetUserByUserBussinessCode(string userBussinessCode)
        {
            User item = dlUser.GetUserByUserBussinessCode(userBussinessCode);
            return Ok(item);
        }

        [HttpGet]
        [Route("GetUserByUserBussinessCodeAllStates")]
        public IHttpActionResult GetUserByUserBussinessCodeAllStates(string userBussinessCode)
        {
            User item = dlUser.GetUserByUserBussinessCodeAllStates(userBussinessCode);
            return Ok(item);
        }

        /*
        [HttpGet]
        [Route("GetUserByName")]
        public IHttpActionResult GetUserByName(string userName)
        {
            User item = dlUser.GetUserByName(userName);
            return Ok(item);
        }
    */
        [HttpPost]
        [Route("save")]
        public IHttpActionResult save(User user)
        {
            //TODO COMPLETAR USER
            int userId = 1;
            user.createdUser = userId;

            bool resul = dlUser.CreateUser(ref user);

            return Ok(resul);
        }

        [HttpPost]
        [Route("doValidateUserExistsCreate")]
        public IHttpActionResult doValidateUserExistsCreate(User user)
        {
            try
            {
                int codeResult = 0;

                codeResult = dlUser.doValidateUserExistsCreate(user);

                return Ok(codeResult);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                throw ex;
            }
        }

        [HttpPost]
        [Route("doValidateUserExistsEdit")]
        public IHttpActionResult doValidateUserExistsEdit(User user)
        {
            try
            {
                int codeResult = 0;

                codeResult = dlUser.doValidateUserExistsEdit(user);

                return Ok(codeResult);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                throw ex;
            }
        }


        //[HttpPost]
        //[Route("update")]
        //public IHttpActionResult update(User user)
        //{
        //    bool resul = dlUser.UpdateUser(ref user);

        //    return Ok(resul);
        //}

        [HttpPost]
        [Route("delete")]
        public IHttpActionResult delete(User user)
        {
            bool resul = dlUser.DeleteUser(ref user);

            return Ok(resul);
        }

        [HttpPost]
        [Route("locked")]
        public IHttpActionResult locked(User user)
        {
            bool resul = dlUser.LockedUser(ref user);

            return Ok(resul);
        }

        [HttpPost]
        [Route("reset")]
        public IHttpActionResult reset(User user)
        {
            bool resul = dlUser.ResetPassword(ref user);

            return Ok(resul);
        }



    }
}
