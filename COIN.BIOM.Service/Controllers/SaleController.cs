﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.query;
using COIN.BIOM.Data;

namespace COIN.BIOM.Service.Controllers
{
    [RoutePrefix("api/sale")]
    public class SaleController : ApiController
    {
        private DLSale oDLSale;

        public SaleController()
        {
            oDLSale = new DLSale();
        }

        [HttpGet]
        [Route("getAllSales")]
        public IHttpActionResult GetAllSales()
        {
            List<SaleQuery> resul = oDLSale.GetAllSales();

            return Ok(resul);
        }






    }
}
