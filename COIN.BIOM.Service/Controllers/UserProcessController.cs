﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.query;
using COIN.BIOM.Data;

namespace COIN.BIOM.Service.Controllers
{
    [RoutePrefix("api/userProcess")]
    public class UserProcessController : ApiController
    {
        private DLUserProcess dlUserProcess;

        public UserProcessController()
        {
            dlUserProcess = new DLUserProcess();
        }

        [HttpPost]
        [Route("save")]
        public IHttpActionResult save(UserProcess userProcess)
        {
            bool resul = dlUserProcess.IncludeUserToProcess(ref userProcess);

            return Ok(resul);
        }


        
        [HttpPost]
        [Route("doValidateUserProcessExists")]
        public IHttpActionResult doValidateUserExists(UserProcess userProcess)
        {
            bool resul = dlUserProcess.doValidateUserProcessExists(userProcess);

            return Ok(resul);
        }
        

        [HttpPost]
        [Route("delete")]
        public IHttpActionResult delete(UserProcess userProcess)
        {
            bool resul = dlUserProcess.ExcludeUserFromProcess(ref userProcess);

            return Ok(resul);
        }

    
    
        

    }
}
