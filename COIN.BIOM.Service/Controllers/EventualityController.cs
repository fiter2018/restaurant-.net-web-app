﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using COIN.BIOM.Data;
using System.Globalization;
using COIN.BIOM.Entity.query;

namespace COIN.BIOM.Service.Controllers
{
    [RoutePrefix("api/eventuality")]
    public class EventualityController : ApiController
    {
        private DLEventLog dlEventLog;

        public EventualityController()
        {
            dlEventLog = new DLEventLog();
        }

        [HttpGet]
        [Route("getEventLogsByParams")]
        public IHttpActionResult GetEventLogsByParams(string date1, string date2, int processId, int applicationId)
        {
            DateTime? odate1 = null;
            DateTime? odate2 = null;

            try
            {
                odate1 = DateTime.ParseExact(date1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                odate2 = DateTime.ParseExact(date2, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch (Exception ex) { }

            List<EventlogQuery> resul = dlEventLog.GetEventlogsByParams(odate1, odate2, processId, applicationId);

            return Ok(resul);
        }









    }
}
