﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using COIN.BIOM.Data;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.query;

namespace COIN.BIOM.Service.Controllers
{
    [RoutePrefix("api/process")]
    public class ProcessController : ApiController
    {
        private DLProcess oDLProcess;

        public ProcessController()
        {
            oDLProcess = new DLProcess();
        }

        [HttpGet]
        [Route("List")]
        public IHttpActionResult List(string code, string name, int applicationId)
        {
            List<ProcessList> lista = oDLProcess.List(code == null ? string.Empty : code, name == null ? string.Empty : name, applicationId);
            return Ok(lista);
        }

        [HttpGet]
        [Route("ListProcessUsersValids")]
        public IHttpActionResult ListProcessUsersValids(string code, string name, int applicationId)
        {
            List<ProcessUsersQuery> lista = oDLProcess.ListProcessUsersValids(code == null ? string.Empty : code, name == null ? string.Empty : name, applicationId);
            return Ok(lista);
        }

        [HttpGet]
        [Route("ListProcessUsers")]
        public IHttpActionResult ListProcessUsers(string code, string name, int applicationId)
        {
            List<ProcessUsersQuery> lista = oDLProcess.ListProcessUsers(code == null ? string.Empty : code, name == null ? string.Empty : name, applicationId);
            return Ok(lista);
        }

        [HttpGet]
        [Route("Get/{id}")]
        public IHttpActionResult Get(string id)
        {
            Process item = oDLProcess.Get(id);
            return Ok(item);
        }

        [HttpPost]
        [Route("Save")]
        public IHttpActionResult Save(Process model)
        {
            //TODO COMPLETAR USER
            bool result = oDLProcess.Save(ref model);
            return Ok(result);
        }

        [HttpPost]
        [Route("Edit")]
        public IHttpActionResult Edit(Process model)
        {
            //TODO COMPLETAR USER
            bool result = oDLProcess.Edit(ref model);
            return Ok(result);
        }

        [HttpPost]
        [Route("doValidateProcessExists")]
        public IHttpActionResult doValidateProcessExists(Process model)
        {
            bool resul = oDLProcess.doValidateProcessExists(model);

            return Ok(resul);
        }

        [HttpPost]
        [Route("Delete")]
        public IHttpActionResult Delete(Process model)
        {
            //TODO COMPLETAR USER
            bool result = oDLProcess.Delete(model.processCode);
            return Ok(result);
        }

        [HttpGet]
        [Route("getProcess")]
        public IHttpActionResult GetProcess()
        {
            List<Process> resul = oDLProcess.GetProcess();

            return Ok(resul);
        }

    }
}
