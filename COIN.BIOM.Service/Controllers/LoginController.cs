﻿using System;
using System.IO;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Hosting;
using COIN.BIOM.Data;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.model;
using BAZ.GEN.Log4Dan;
using COIN.BIOM.Util;


namespace COIN.BIOM.Service.Controllers
{
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        private DLAcceso oDLAcceso;
        private Log Log = new Log(typeof(LoginController));

        public LoginController()
        {
            oDLAcceso = new DLAcceso();
        }

        [HttpPost]
        [Route("doLogin")]
        public IHttpActionResult doLogin(Login datos)
        {
            try
            {
                int codeResult = 0;

                codeResult = oDLAcceso.getLogin(datos);

                return Ok(codeResult);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                throw ex;
            }
        }

        
        [HttpPost]
        [Route("doRecoveryPass")]
        public IHttpActionResult doRecoveryPass(Login datos)
        {
            try
            {
                int codeResult = 0;
                SalidaWeb response = new SalidaWeb();

                codeResult = oDLAcceso.getRecoveryPass(datos);

                if (codeResult == Constants.ErrorUsuarioNoExiste)
                {
                    response.code = 1;
                    response.message = "No Existe el Usuario";
                    response.data = string.Empty;
                }
                else if (codeResult == Constants.ErrorUsuarioNoActivo)
                {
                    response.code = 1;
                    response.message = "El Usuario no se encuentra activo";
                    response.data = string.Empty;
                }
                else if (codeResult == Constants.ErrorUsuarioMozo)
                {
                    response.code = 1;
                    response.message = "El Usuario no cuenta con los permisos";
                    response.data = string.Empty;
                }
                else
                {
                    User user = oDLAcceso.GetUserByEmail(datos.email);
                    string token = oDLAcceso.CreateToken(Constants.TipoRecuperacionClave, user.userId);

                    //Enviar correo de recuperación - STN
                    BIOMUtil.EnviarCorreo(GenerarCorreoRecuperarClave(user, token.Substring(token.Length - 6)));

                    response.code = 0;
                    response.message = "OK";
                    response.data = token;
                }

                return Ok(response);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                throw ex;
            }
        }
        

        [HttpPost]
        [Route("doValidateCode")]
        public IHttpActionResult doValidateCode(Login model)
        {
            try
            {
                int codeResult = 0;
                SalidaWeb response = new SalidaWeb();

                codeResult = oDLAcceso.getValidateCode(model);

                if (codeResult == Constants.ErrorCodigoToken)
                {
                    response.code = 1;
                    response.message = "No Existe el Codigo Token";
                    response.data = string.Empty;
                }
                else if (codeResult == Constants.ErrorCodigoRecuperacion)
                {
                    response.code = 1;
                    response.message = "Codigo de Recuperación Incorrecto";
                    response.data = string.Empty;
                }
                else if (codeResult == Constants.ErrorVigenciaCodigoRecuperacion)
                {
                    response.code = 1;
                    response.message = "El Codigo de Recuperación ha Expirado";
                    response.data = string.Empty;
                }
                else
                {
                    response.code = 0;
                    response.message = "OK";
                    response.data = string.Empty;
                }

                return Ok(response);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                throw ex;
            }
        }

        [HttpPost]
        [Route("setResetPass")]
        public IHttpActionResult setResetPass(Login model)
        {
            try
            {
                SalidaWeb response = new SalidaWeb();

                if (model.nuevaClave != model.confirmaClave)
                {
                    response.code = 1;
                    response.message = "El Password de Confirmación No Coincide";
                    response.data = string.Empty;
                }
                else
                {
                    int codeResult = 0;

                    codeResult = oDLAcceso.getResetPass(model);

                    if (codeResult == Constants.ErrorCodigoToken)
                    {
                        response.code = 1;
                        response.message = "No Existe el Codigo Token";
                        response.data = string.Empty;
                    }
                    else if (codeResult == Constants.ErrorVigenciaToken)
                    {
                        response.code = 1;
                        response.message = "El Codigo Token ha Expirado";
                        response.data = string.Empty;
                    }
                    else if (codeResult == Constants.ErrorNoExisteIdUserXToken)
                    {
                        response.code = 1;
                        response.message = "No Existe Usuario para el Token Ingresado";
                        response.data = string.Empty;
                    }
                    else
                    {
                        response.code = 0;
                        response.message = "Se ha Actualizado el Password";
                        response.data = string.Empty;
                    }
                }

                return Ok(response);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                throw ex;
            }
        }

        private MailModel GenerarCorreoRecuperarClave(User user, string recoveryCode)
        {
            string mensajeTexto = string.Empty;
            string mensajeHTML = string.Empty;
            MailModel resul = new MailModel();

            string URLSite = ConfigurationManager.AppSettings["URLSITEWEB"];

            string plantillahtml = HostingEnvironment.MapPath("~/PlantillasCorreo/RecoveryPass.html");
            string plantillatxt = HostingEnvironment.MapPath("~/PlantillasCorreo/RecoveryPass.txt");

            //Mensaje HTML
            StreamReader sr = new StreamReader(plantillahtml);
            mensajeHTML = sr.ReadToEnd();
            sr.Close();

            mensajeHTML = mensajeHTML.Replace("{%USUARIO%}", string.Format("{0} {1}", user.name, user.lastName));
            mensajeHTML = mensajeHTML.Replace("{%RECOVERYCODE%}", recoveryCode);

            //Mensaje Texto
            sr = new StreamReader(plantillatxt);
            mensajeTexto = sr.ReadToEnd();
            sr.Close();

            mensajeTexto = mensajeTexto.Replace("{%USUARIO%}", string.Format("{0} {1}", user.name, user.lastName));
            mensajeTexto = mensajeTexto.Replace("{%RECOVERYCODE%}", recoveryCode);

            resul.Para.Add(user.email);
            resul.Asunto = Constants.AsuntoRecuperarClave;
            resul.MensajeTexto = mensajeTexto;
            resul.MensajeHtml = mensajeHTML;

            return resul;
        }



    }
}