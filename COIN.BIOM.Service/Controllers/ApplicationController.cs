﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using COIN.BIOM.Data;
using COIN.BIOM.Entity;

namespace COIN.BIOM.Service.Controllers
{
    [RoutePrefix("api/application")]
    public class ApplicationController : ApiController
    {
        private DLApplication dlApplication;

        public ApplicationController()
        {
            dlApplication = new DLApplication();
        }

        [HttpGet]
        [Route("getApplication")]
        public IHttpActionResult GetApplication()
        {
            List<Application> resul = dlApplication.GetApplication();

            return Ok(resul);
        }

        [HttpPost]
        [Route("doValidateApplicationExists")]
        public IHttpActionResult doValidateApplicationExists(Application app)
        {
            bool resul = dlApplication.doValidateApplicationExists(app);

            return Ok(resul);
        }




    }
}
