﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COIN.BIOM.Clients;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.query;

namespace COIN.BIOM.Business
{
    public class BLProcess
    {
        private BIOMClient oBIOMClient;

        public BLProcess()
        {
            oBIOMClient = new BIOMClient();
        }


        public List<ProcessList> List(string code, string name, int applicationId)
        {
            try
            {
                return oBIOMClient.Get<List<ProcessList>>(string.Format("process/list?code={0}&name={1}&applicationId={2}", code, name, applicationId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProcessUsersQuery> ListProcessUsersValids(string code, string name, int applicationId)
        {
            try
            {
                return oBIOMClient.Get<List<ProcessUsersQuery>>(string.Format("process/listProcessUsersValids?code={0}&name={1}&applicationId={2}", code, name, applicationId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProcessUsersQuery> ListProcessUsers(string code, string name, int applicationId)
        {
            try
            {
                return oBIOMClient.Get<List<ProcessUsersQuery>>(string.Format("process/listProcessUsers?code={0}&name={1}&applicationId={2}", code, name, applicationId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ProcessList Get(string processCode)
        {
            try
            {
                return oBIOMClient.Get<ProcessList>(string.Format("process/get/{0}", processCode));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Save(Process process)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<Process>("process/save", process));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Edit(Process process)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<Process>("process/edit", process));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool doValidateProcessExists(Process process)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<Process>("process/doValidateProcessExists", process));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool Delete(Process process)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<Process>("process/delete", process));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Process> GetProcess()
        {
            try
            {
                return oBIOMClient.Get<List<Process>>(string.Format("process/getProcess"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }











    }
}
