﻿using COIN.BIOM.Clients;
using COIN.BIOM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Business
{
    public class BLEnroll
    {
        private BIOMClient oBIOMClient;

        public BLEnroll()
        {
            oBIOMClient = new BIOMClient();
        }

        public bool Save(Enroll model)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<Enroll>("enroll/save", model));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Enroll> GetEnrolls()
        {
            try
            {
                return oBIOMClient.Get<List<Enroll>>("enroll/getEnrolls");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Verify(Enroll model)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<Enroll>("enroll/verify", model));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
