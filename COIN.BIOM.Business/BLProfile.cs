﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COIN.BIOM.Clients;
using COIN.BIOM.Entity;

namespace COIN.BIOM.Business
{
    public class BLProfile
    {
        private BIOMClient oBIOMClient;

        public BLProfile()
        {
            oBIOMClient = new BIOMClient();
        }

        public List<Profile> GetProfile()
        {
            try
            {
                return oBIOMClient.Get<List<Profile>>(string.Format("profile/getProfile"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
