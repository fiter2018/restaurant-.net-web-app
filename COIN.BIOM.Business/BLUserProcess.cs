﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COIN.BIOM.Entity;
using COIN.BIOM.Clients;

namespace COIN.BIOM.Business
{
    public class BLUserProcess
    {
        private BIOMClient oBIOMClient;

        public BLUserProcess()
        {
            oBIOMClient = new BIOMClient();
        }

        public bool Save(UserProcess userProcess)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<UserProcess>("userProcess/save", userProcess));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool doValidateUserProcessExists(UserProcess userProcess)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<UserProcess>("userProcess/doValidateUserProcessExists", userProcess));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(UserProcess userProcess)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<UserProcess>("userProcess/delete", userProcess));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }











    }
}
