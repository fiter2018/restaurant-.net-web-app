﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COIN.BIOM.Clients;
using COIN.BIOM.Entity;
using Newtonsoft.Json;


namespace COIN.BIOM.Business
{
    public class BLAcceso
    {
        private BIOMClient oBIOMClient;

        public BLAcceso()
        {
            oBIOMClient = new BIOMClient();
        }

        public int ValidarAcceso(Login datos)
        {
            try
            {
                return Convert.ToInt32(oBIOMClient.Post<Login>("login/doLogin", datos));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SalidaWeb RecuperarClave(Login datos)
        {
            try
            {
                return JsonConvert.DeserializeObject<SalidaWeb>(oBIOMClient.Post<Login>("login/doRecoveryPass", datos));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SalidaWeb VerificarToken(Login datos)
        {
            try
            {
                return JsonConvert.DeserializeObject<SalidaWeb>(oBIOMClient.Post<Login>("login/doValidateCode", datos));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public SalidaWeb ReestablecerClave(Login datos)
        {
            try
            {
                return JsonConvert.DeserializeObject<SalidaWeb>(oBIOMClient.Post<Login>("login/setResetPass", datos));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
