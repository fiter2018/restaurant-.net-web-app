﻿using COIN.BIOM.Clients;
using COIN.BIOM.Entity.model;
using COIN.BIOM.Entity.query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Business
{
    public class BLComponent
    {

        private BIOMClient oBIOMClient;

        public BLComponent()
        {
            oBIOMClient = new BIOMClient();
        }

        public UserEnrollQuery GetUserEnroll(string userBussinessCode, string tokenCode)
        {
            try
            {
                return oBIOMClient.Get<UserEnrollQuery>(string.Format("component/getUserEnroll?tokenCode={0}&userBussinessCode={1}", tokenCode, userBussinessCode));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserEnrollQuery> GetUserEnrollsByProfile(string tokenCode, int profileId)
        {
            try
            {
                return oBIOMClient.Get<List<UserEnrollQuery>>(string.Format("component/getUserEnrollsByProfile?tokenCode={0}&profileId={1}", tokenCode, profileId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveEnroll(EnrollModel model)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<EnrollModel>("component/saveFingertemplate", model));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ResponseGenericModel SaveCompleteUser(EnrollUserModel model)
        {
            try
            {
                return JsonConvert.DeserializeObject<ResponseGenericModel>(oBIOMClient.Post<EnrollUserModel>("component/SaveCompleteUser", model));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveEventLog(EventLogModel model)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<EventLogModel>("component/SaveEventLog", model));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
