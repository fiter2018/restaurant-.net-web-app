﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COIN.BIOM.Clients;
using COIN.BIOM.Entity.query;

namespace COIN.BIOM.Business
{
    public class BLEventuality
    {

        private BIOMClient oBIOMClient;

        public BLEventuality()
        {
            oBIOMClient = new BIOMClient();
        }

        public List<EventlogQuery> GetEventlogsByParams(DateTime? Date1, DateTime? Date2, int ProcessId, int ApplicationId)
        {
            try
            {
                string CDate1 = Date1.HasValue ? string.Format("{0:dd/MM/yyyy}", Date1) : string.Empty;
                string CDate2 = Date2.HasValue ? string.Format("{0:dd/MM/yyyy}", Date2) : string.Empty;

                return oBIOMClient.Get<List<EventlogQuery>>(string.Format("eventuality/getEventLogsByParams?date1={0}&date2={1}&processId={2}&applicationId={3}", CDate1, CDate2, ProcessId, ApplicationId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }









    }
}
