﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COIN.BIOM.Clients;
using COIN.BIOM.Entity;

namespace COIN.BIOM.Business
{
    public class BLApplication
    {
        private BIOMClient oBIOMClient;

        public BLApplication()
        {
            oBIOMClient = new BIOMClient();
        }

        public List<Application> GetApplication()
        {
            try
            {
                return oBIOMClient.Get<List<Application>>(string.Format("application/getApplication"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool doValidateApplicationExists(Application app)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<Application>("application/doValidateApplicationExists", app));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
