﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COIN.BIOM.Clients;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.query;

namespace COIN.BIOM.Business
{
    public class BLUser
    {
        private BIOMClient oBIOMClient;

        public BLUser()
        {
            oBIOMClient = new BIOMClient();
        }

        public List<UserQuery> GetUserByParams(string CodUsuario, string NombUsuario, int Dni, int Perfil)
        {
            try
            {
                return oBIOMClient.Get<List<UserQuery>>(string.Format("user/getUserByParams?CodUsuario={0}&NombUsuario={1}&Dni={2}&Perfil={3}", CodUsuario, NombUsuario, Dni, Perfil));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserQuery> getUserByProfileCustomer(int customerID, string profileIds)
        {
            try
            {
                return oBIOMClient.Get<List<UserQuery>>(string.Format("user/getUserByProfileCustomer?customerID={0}&profileIds={1}", customerID, profileIds));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<User> getAllUsers()
        {
            try
            {
                return oBIOMClient.Get<List<User>>(string.Format("user/getAllUsers"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Save(User user)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<User>("user/save", user));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int doValidateUserExistsCreate(User user)
        {
            try
            {
                return Convert.ToInt32(oBIOMClient.Post<User>("user/doValidateUserExistsCreate", user));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int doValidateUserExistsEdit(User user)
        {
            try
            {
                return Convert.ToInt32(oBIOMClient.Post<User>("user/doValidateUserExistsEdit", user));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User Get(string userCode)
        {
            try
            {
                return oBIOMClient.Get<User>(string.Format("user/get/{0}", userCode));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User GetUserByName(string userName)
        {
            try
            {
                return oBIOMClient.Get<User>(string.Format("user/GetUserByName?userName={0}", userName));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User GetUserByUserBussinessCode(string userBussinessCode)
        {
            try
            {
                return oBIOMClient.Get<User>(string.Format("user/GetUserByUserBussinessCode?userBussinessCode={0}", userBussinessCode));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User GetUserByUserBussinessCodeAllStates(string userBussinessCode)
        {
            try
            {
                return oBIOMClient.Get<User>(string.Format("user/GetUserByUserBussinessCodeAllStates?userBussinessCode={0}", userBussinessCode));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public bool Update(User user)
        //{
        //    try
        //    {
        //        return Convert.ToBoolean(oBIOMClient.Post<User>("user/update", user));
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public bool Delete(User user)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<User>("user/delete", user));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Locked(User user)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<User>("user/locked", user));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Reset(User user)
        {
            try
            {
                return Convert.ToBoolean(oBIOMClient.Post<User>("user/reset", user));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
