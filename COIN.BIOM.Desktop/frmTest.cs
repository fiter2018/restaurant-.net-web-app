﻿using COIN.BIOM.Entity.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace COIN.BIOM.Desktop
{
    public partial class frmTest : Form
    {
        public frmTest()
        {
            InitializeComponent();
        }

        private void btnLanzar_Click(object sender, EventArgs e)
        {
            SessionModel session = new SessionModel();

            session.tokenCode = ConfigurationManager.AppSettings["TOKEN"];
            session.processBussinessCode = txtProcessCode.Text;
            session.applicationBussinessCode = txtApplicationCode.Text;
            session.userBussinessCode = txtUserCode.Text;

            Session.SetSession(session);

            frmMain main = new frmMain();
            main.ShowDialog();
        }
    }
}
