﻿using COIN.BIOM.Business;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.model;
using COIN.BIOM.Entity.query;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace COIN.BIOM.Desktop
{
    public partial class frmMain : Form, DPFP.Capture.EventHandler
    {
        delegate void Function();
        private DPFP.Capture.Capture Capturer;
        private DPFP.Template Template;
        private DPFP.Verification.Verification Verificator;
        private BLComponent oDLComponente;
        private List<EnrollQuery> enrolls;

        public frmMain()
        {
            InitializeComponent();
            oDLComponente = new BLComponent();

        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Stop();
            frmEnrollment enrollment = new frmEnrollment();
            enrollment.Cargar();
            Start();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            //tomar los datos y generar elJSON
            List<UserEnrollQuery> userEnrolls = oDLComponente.GetUserEnrollsByProfile(Session.GetSession().tokenCode, 0);

            SaveJSON(userEnrolls);

            //obtener los datos del usuario que se esta enviando
            UserEnrollQuery userEnroll = oDLComponente.GetUserEnroll(Session.GetSession().userBussinessCode, Session.GetSession().tokenCode);

            txtCode.Text = Session.GetSession().userBussinessCode;
            if (userEnroll != null)
            {
                txtFullName.Text = userEnroll.fullName;
                txtDNI.Text = userEnroll.dni;

                if (userEnroll.fingersTemplate.Count > 0)
                {
                    this.enrolls = userEnroll.fingersTemplate;
                    lblMensajeHuellas.Text = "Coloque su huella";
                    Init();
                    Start();
                }
                else
                {
                    lblMensajeHuellas.Text = "Usuario no cuenta con huellas";
                }
            }
            else
            {
                SetStatus("Usuario No Existe", null);
            }
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            Stop();
            System.Windows.Forms.Application.ExitThread();
        }

        public void OnComplete(object Capture, string ReaderSerialNumber, DPFP.Sample Sample)
        {
            //MakeReport("The fingerprint sample was captured.");
            //SetPrompt("Scan the same fingerprint again.");
            Process(Sample);
        }

        public void OnFingerGone(object Capture, string ReaderSerialNumber)
        {
            //MakeReport("The finger was removed from the fingerprint reader.");
        }

        public void OnFingerTouch(object Capture, string ReaderSerialNumber)
        {
            //MakeReport("The fingerprint reader was touched.");
        }

        public void OnReaderConnect(object Capture, string ReaderSerialNumber)
        {
            //MakeReport("The fingerprint reader was connected.");
        }

        public void OnReaderDisconnect(object Capture, string ReaderSerialNumber)
        {
            //MakeReport("The fingerprint reader was disconnected.");
        }

        public void OnSampleQuality(object Capture, string ReaderSerialNumber, DPFP.Capture.CaptureFeedback CaptureFeedback)
        {
            /*
            if (CaptureFeedback == DPFP.Capture.CaptureFeedback.Good)
                MakeReport("The quality of the fingerprint sample is good.");
            else
                MakeReport("The quality of the fingerprint sample is poor.");
                */
        }


        protected virtual void Init()
        {
            try
            {
                Capturer = new DPFP.Capture.Capture();				// Create a capture operation.

                if (null != Capturer)
                    Capturer.EventHandler = this;                   // Subscribe for capturing events.
                                                                    //else
                                                                    //SetPrompt("Can't initiate capture operation!")
                Verificator = new DPFP.Verification.Verification();     // Create a fingerprint template verificator
                //UpdateStatus(0);
            }
            catch
            {
                MessageBox.Show("Can't initiate capture operation!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        protected void Start()
        {
            if (null != Capturer)
            {
                try
                {
                    Capturer.StartCapture();
                    //SetPrompt("Using the fingerprint reader, scan your fingerprint.");
                }
                catch
                {
                    //SetPrompt("Can't initiate capture!");
                }
            }
        }

        protected void Stop()
        {
            if (null != Capturer)
            {
                try
                {
                    Capturer.StopCapture();
                }
                catch
                {
                    //SetPrompt("Can't terminate capture!");
                }
            }
        }


        protected virtual void Process(DPFP.Sample Sample)
        {
            // Draw fingerprint sample image.
            DrawPicture(ConvertSampleToBitmap(Sample));

            // Process the sample and create a feature set for the enrollment purpose.
            DPFP.FeatureSet features = ExtractFeatures(Sample, DPFP.Processing.DataPurpose.Verification);

            // Check quality of the sample and start verification if it's good
            // TODO: move to a separate task
            if (features != null)
            {
                EventLogModel eventLog = new EventLogModel();
                eventLog.userBussinessCode = Session.GetSession().userBussinessCode;
                eventLog.tokenCode  = Session.GetSession().tokenCode;
                eventLog.processBussinessCode = Session.GetSession().processBussinessCode;
                eventLog.eventDateTime = string.Format("{0:dd/MM/yyyy HH:mm:ss}", DateTime.Now);
                

                List<EnrollQuery> enrolls = this.enrolls;

                // Compare the feature set with our template
                foreach(EnrollQuery itm in enrolls)
                {
                    Stream stream = new MemoryStream(itm.fingerTemplate);
                    Template = new DPFP.Template(stream);

                    DPFP.Verification.Verification.Result result = new DPFP.Verification.Verification.Result();
                    Verificator.Verify(features, Template, ref result);

                    if (result.Verified)
                    {
                        SetStatus("La huella fue VERIFICADA.", itm);
                        eventLog.eventResult = 1;
                        if (!oDLComponente.SaveEventLog(eventLog))
                        {
                            //log para guardar errores
                        }
                        return;
                    }
                }
                SetStatus("La Huella NO FUE VERIFICADA.", null);
                eventLog.eventResult = 2;
                if (!oDLComponente.SaveEventLog(eventLog))
                {
                    //log para guardar errores
                }
            }
        }

        protected Bitmap ConvertSampleToBitmap(DPFP.Sample Sample)
        {
            DPFP.Capture.SampleConversion Convertor = new DPFP.Capture.SampleConversion();  // Create a sample convertor.
            Bitmap bitmap = null;                                                           // TODO: the size doesn't matter
            Convertor.ConvertToPicture(Sample, ref bitmap);                                 // TODO: return bitmap as a result
            return bitmap;
        }

        private void DrawPicture(Bitmap bitmap)
        {
            this.Invoke(new Function(delegate () {
                Picture.Image = new Bitmap(bitmap, Picture.Size);   // fit the image into the picture box
            }));
        }

        protected DPFP.FeatureSet ExtractFeatures(DPFP.Sample Sample, DPFP.Processing.DataPurpose Purpose)
        {
            DPFP.Processing.FeatureExtraction Extractor = new DPFP.Processing.FeatureExtraction();  // Create a feature extractor
            DPFP.Capture.CaptureFeedback feedback = DPFP.Capture.CaptureFeedback.None;
            DPFP.FeatureSet features = new DPFP.FeatureSet();

            Extractor.CreateFeatureSet(Sample, Purpose, ref feedback, ref features);            // TODO: return features as a result?
            if (feedback == DPFP.Capture.CaptureFeedback.Good)
                return features;
            else
                return null;
        }


        protected void SetStatus(string status, EnrollQuery model)
        {
            if (model != null)
            {
                this.Invoke(new Function(delegate ()
                {
                    lblResultado.ForeColor = Color.Green;
                }));
            }else
            {
                this.Invoke(new Function(delegate ()
                {
                    lblResultado.ForeColor = Color.Red;
                }));
            }

            this.Invoke(new Function(delegate ()
            {
                lblResultado.Text = status;
            }));
        }

        private void btnRegistroHuellas_Click(object sender, EventArgs e)
        {
            Stop();
            frmUpdateEnrollment FrmUpdate = new frmUpdateEnrollment();
            FrmUpdate.Cargar();
            Start();
        }

        private void SaveJSON(List<UserEnrollQuery> userEnrolls)
        {
            string rutaJSON = Path.Combine(System.Windows.Forms.Application.StartupPath, "Templates.json");
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(rutaJSON ))
            {
                string serialize = JsonConvert.SerializeObject(userEnrolls);
                file.WriteLine(serialize);
                file.Close();
            }

        }

    }
}
