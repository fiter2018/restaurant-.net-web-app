﻿using COIN.BIOM.Entity.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Desktop
{
    public static class Session
    {
        private static SessionModel session {get; set;}

        public static void SetSession(SessionModel model)
        {
            session = model;
        }

        public static SessionModel GetSession()
        {
            return session;
        }

    }
}
