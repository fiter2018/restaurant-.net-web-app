﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COIN.BIOM.Entity;

namespace COIN.BIOM.Data
{
    public class DLApplication
    {

        private BIOMConexion BIOMConexion = new BIOMConexion();

        public List<Application> GetApplication()
        {
            try
            {
                List<Application> resul = new List<Application>();

                string strsql = "usp_GetApplication";
                List<SqlParameter> parametros = new List<SqlParameter>();
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, null);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Application obj = new Application();

                        obj.applicationId = Convert.ToInt32(reader["applicationId"]);
                        obj.applicationBussinessCode = Convert.ToString(reader["applicationBussinessCode"]);
                        obj.applicationName = Convert.ToString(reader["applicationName"]);

                        resul.Add(obj);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool doValidateApplicationExists(Application objApplication)
        {
            try
            {
                bool resul = false;

                string strsql = "usp_doValidateApplicationExists";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@applicationBussinessCode", System.Data.SqlDbType.VarChar, 10, objApplication.applicationBussinessCode));

                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        resul = true;
                        break;
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
