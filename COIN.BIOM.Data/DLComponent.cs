﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using COIN.BIOM.Entity;
using System.Data;

namespace COIN.BIOM.Data
{
    public class DLComponent
    {

        private BIOMConexion BIOMConexion = new BIOMConexion();

        public bool Save(Enroll model)
        {
            try
            {

                string strsql = "dbo.usp_SaveEnroll";
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userId", System.Data.SqlDbType.Int, model.userId));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@FingerTemplate", System.Data.SqlDbType.Image, model.FingerTemplate));
                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int doValidatePreRegisterUser(User user, Process process)
        {
            try
            {
                int codeResult = 0;
                string strsql = "usp_doValidatePreRegisterUserComponent";
                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter parametroSalida = BIOMConexion.GetUtilBD().CreateParameter("@codeResult", System.Data.SqlDbType.Int, codeResult);
                parametroSalida.Direction = ParameterDirection.InputOutput;

                parametros.Add(parametroSalida);
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userBussinessCode", System.Data.SqlDbType.VarChar, 10, user.userBussinessCode));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processBussinessCode", System.Data.SqlDbType.VarChar, 10, process.processBussinessCode));

                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                codeResult = Convert.ToInt32(parametroSalida.Value);

                return codeResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

















    }

}
