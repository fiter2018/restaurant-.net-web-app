﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.query;

namespace COIN.BIOM.Data
{
    public class DLEventLog
    {
        private BIOMConexion BIOMConexion = new BIOMConexion();

        public List<EventlogQuery> GetEventlogsByParams(DateTime? Date1, DateTime? Date2, int ProcessId, int ApplicationId)
        {
            try
            {
                List<EventlogQuery> resul = new List<EventlogQuery>();

                string strsql = "usp_GetEventLogsByParams";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@Date1", System.Data.SqlDbType.DateTime, Date1));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@Date2", System.Data.SqlDbType.DateTime, Date2));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@ProcessId", System.Data.SqlDbType.Int, ProcessId));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@ApplicationId", System.Data.SqlDbType.Int, ApplicationId));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        EventlogQuery obj = new EventlogQuery();

                        obj.userBussinessCode = Convert.ToString(reader["userBussinessCode"]);
                        obj.userName = Convert.ToString(reader["userName"]);
                        obj.completeName = Convert.ToString(reader["completeName"]);

                        String profileName = "--";
                        if (reader["profileId"] != DBNull.Value)
                        {
                            switch (Convert.ToInt32(reader["profileId"]))
                            {
                                case 1: profileName = "Administrador"; break;
                                case 2: profileName = "Responsable Sucursal"; break;
                                case 3: profileName = "Usuario"; break;
                            }
                        }

                        obj.profileName = profileName;
                        obj.dni = Convert.ToInt32(reader["dni"]);
                        obj.email = Convert.ToString(reader["email"]);

                        String userState = "--";
                        if (reader["state"] != DBNull.Value)
                        {
                            switch (Convert.ToInt32(reader["state"]))
                            {
                                case 0: userState = "Inactivo"; break;
                                case 1: userState = "Activo"; break;
                            }
                        }

                        obj.userState = userState;
                        obj.applicationBussinessCode = Convert.ToString(reader["applicationBussinessCode"]);
                        obj.applicationName = Convert.ToString(reader["applicationName"]);
                        obj.processBussinessCode = Convert.ToString(reader["processBussinessCode"]);
                        obj.processName = Convert.ToString(reader["processName"]);
                        obj.eventlogDatatime = Convert.ToDateTime(reader["eventDatetime"]);

                        String eventResult = "--";
                        if (reader["eventResult"] != DBNull.Value)
                        {
                            switch (Convert.ToInt32(reader["eventResult"]))
                            {
                                case 0: eventResult = "Marcado Incorrecto"; break;
                                case 1: eventResult = "Marcado Correcto"; break;
                            }
                        }

                        obj.eventlogResult = eventResult;

                        resul.Add(obj);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool Save(EventLog eventLog)
        {
            try
            {

                string strsql = "dbo.usp_SaveEventLog";
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userId", System.Data.SqlDbType.Int, eventLog.userId));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processId", System.Data.SqlDbType.Int, eventLog.processId));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@eventDateTime", System.Data.SqlDbType.DateTime, eventLog.eventDatetime));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@eventResult", System.Data.SqlDbType.Int, eventLog.eventResult));

                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }









    }
}
