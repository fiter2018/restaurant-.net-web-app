﻿using COIN.BIOM.Entity;
using COIN.BIOM.Entity.query;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Data
{
    public class DLEnroll
    {
        
        private BIOMConexion BIOMConexion = new BIOMConexion();

        public bool Save(Enroll model)
        {
            try
            {

                string strsql = "dbo.usp_SaveEnroll";
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userId", System.Data.SqlDbType.Int, model.userId));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@FingerTemplate", System.Data.SqlDbType.Image, model.FingerTemplate));
                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EnrollQuery> GetEnrollByUser(int UserId)
        {
            try
            {

                string strsql = "dbo.usp_GetEnrollByUser";

                List<EnrollQuery> resul = new List<EnrollQuery>();
                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userId", System.Data.SqlDbType.Int, UserId));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        EnrollQuery enroll = new EnrollQuery();
                        enroll.fingerTemplate = (byte[])(reader["FingerTemplate"]);

                        resul.Add(enroll);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();
                return resul;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserEnrollQuery> GetUserWithEnrollByProfile(int ProfileId)
        {
            try
            {

                string strsql = "dbo.usp_GetUserWithEnrollByProfile";

                List<UserEnrollQuery> resul = new List<UserEnrollQuery>();

                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@ProfileId", System.Data.SqlDbType.Int, ProfileId));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        UserEnrollQuery enroll = new UserEnrollQuery();
                        enroll.userCode = Convert.ToString(reader["userCode"]);
                        enroll.name = Convert.ToString(reader["name"]);
                        enroll.lastName = Convert.ToString(reader["lastName"]);
                        enroll.dni = Convert.ToString(reader["dni"]);


                        resul.Add(enroll);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();
                return resul;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<EnrollQuery> GetUserEnrollByProfile(int ProfileId)
        {
            try
            {

                string strsql = "dbo.usp_GetUserEnrollByProfile";

                List<EnrollQuery> resul = new List<EnrollQuery>();

                List<SqlParameter> parametros = new List<SqlParameter>();

                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@ProfileId", System.Data.SqlDbType.Int, ProfileId));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        EnrollQuery enroll = new EnrollQuery();
                        enroll.userCode = Convert.ToString(reader["userCode"]);
                        enroll.fingerTemplate = (byte[])(reader["FingerTemplate"]);

                        resul.Add(enroll);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();
                return resul;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /*
        public List<Enroll> GetEnrolls()
        {
            try
            {

                string strsql = "dbo.usp_GetEnrolls";

                List<Enroll> resul = new List<Enroll>();
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, null);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Enroll enroll = new Enroll();
                        enroll.Code = Convert.ToString(reader["Code"]);
                        enroll.Name = Convert.ToString(reader["Name"]);
                        enroll.LastName = Convert.ToString(reader["LastName"]);
                        enroll.FingerTemplate = (byte[])(reader["FingerTemplate"]);

                        resul.Add(enroll);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();
                return resul;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    */


    }
}
