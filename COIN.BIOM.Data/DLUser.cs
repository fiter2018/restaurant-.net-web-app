﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.query;
using System.Data;


namespace COIN.BIOM.Data
{
    public class DLUser
    {
        private BIOMConexion BIOMConexion = new BIOMConexion();

        public List<User> GetAllUsers()
        {
            try
            {
                List<User> resul = new List<User>();

                string strsql = "usp_GetAllUsers";
                List<SqlParameter> parametros = new List<SqlParameter>();
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, null);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        User objUser = new User();

                        objUser.userId = Convert.ToInt32(reader["userId"]);
                        objUser.userCode = Convert.ToString(reader["userCode"]);
                        objUser.userName = Convert.ToString(reader["userName"]);
                        objUser.name = Convert.ToString(reader["name"]);
                        objUser.lastName = Convert.ToString(reader["lastName"]);
                        objUser.profileId = Convert.ToInt32(reader["profileId"]);
                        objUser.dni = Convert.ToInt32(reader["dni"]);
                        objUser.emisorId = Convert.ToInt32(reader["emisorId"]);
                        objUser.email = Convert.ToString(reader["email"]);
                        objUser.state = Convert.ToInt32(reader["state"]);
                        objUser.flagLocked = Convert.ToInt32(reader["isLocked"]);

                        resul.Add(objUser);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<UserQuery> getUserByParams(string CodUsuario, string NombUsuario, int Dni, int Perfil)
        {
            try
            {
                List<UserQuery> resul = new List<UserQuery>();

                string strsql = "usp_GetUserByParams";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userBussinessCode", System.Data.SqlDbType.VarChar, 10, CodUsuario));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@name", System.Data.SqlDbType.VarChar, 100, NombUsuario));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@dni", System.Data.SqlDbType.Int, Dni));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@profileId", System.Data.SqlDbType.Int, Perfil));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        UserQuery obj = new UserQuery();

                        obj.flagLocked = Convert.ToInt32(reader["isLocked"]);
                        obj.userCode = Convert.ToString(reader["userCode"]);
                        obj.userBussinessCode = Convert.ToString(reader["userBussinessCode"]);
                        obj.name = Convert.ToString(reader["name"]);
                        obj.lastName = Convert.ToString(reader["lastName"]);
                        obj.userName = Convert.ToString(reader["userName"]);
                        obj.profileName = Convert.ToString(reader["profileName"]);
                        obj.dni = Convert.ToInt32(reader["dni"]);
                        obj.state = Convert.ToInt32(reader["state"]);
                        obj.email = Convert.ToString(reader["email"]);
                        obj.createdDate = Convert.ToDateTime(reader["createdDate"]);

                        resul.Add(obj);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User GetUserByName(string userName)
        {
            try
            {
                User objUser = null;

                string strsql = "usp_GetUserByName";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userName", System.Data.SqlDbType.VarChar, 50, userName));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objUser = new User();
                        objUser.userId = Convert.ToInt32(reader["userId"]);
                        objUser.userCode = Convert.ToString(reader["userCode"]);
                        objUser.userName = Convert.ToString(reader["userName"]);
                        objUser.name = Convert.ToString(reader["name"]);
                        objUser.lastName = Convert.ToString(reader["lastName"]);
                        objUser.profileId = Convert.ToInt32(reader["profileId"]);
                        objUser.dni = Convert.ToInt32(reader["dni"]);
                        objUser.email = Convert.ToString(reader["email"]);
                        objUser.state = Convert.ToInt32(reader["state"]);
                        objUser.flagLocked = Convert.ToInt32(reader["isLocked"]);

                    }
                }
                reader.Close();

                BIOMConexion.CloseConexion();

                return objUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }        

        public User GetUserByUserBussinessCode(string userBussinessCode)
        {
            try
            {
                User objUser = null;

                string strsql = "usp_GetUserByUserBussinessCode";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userBussinessCode", System.Data.SqlDbType.VarChar, 10, userBussinessCode));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objUser = new User();
                        objUser.userId = Convert.ToInt32(reader["userId"]);
                        objUser.userCode = Convert.ToString(reader["userCode"]);
                        objUser.userName = Convert.ToString(reader["userName"]);
                        objUser.name = Convert.ToString(reader["name"]);
                        objUser.lastName = Convert.ToString(reader["lastName"]);
                        objUser.profileId = Convert.ToInt32(reader["profileId"]);
                        objUser.dni = Convert.ToInt32(reader["dni"]);
                        objUser.email = Convert.ToString(reader["email"]);
                        objUser.state = Convert.ToInt32(reader["state"]);
                        objUser.flagLocked = Convert.ToInt32(reader["isLocked"]);

                    }
                }
                reader.Close();

                BIOMConexion.CloseConexion();

                return objUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User GetUserByUserBussinessCodeAllStates(string userBussinessCode)
        {
            try
            {
                User objUser = null;

                string strsql = "usp_GetUserByUserBussinessCodeAllStates";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userBussinessCode", System.Data.SqlDbType.VarChar, 10, userBussinessCode));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objUser = new User();
                        objUser.userId = Convert.ToInt32(reader["userId"]);
                        objUser.userCode = Convert.ToString(reader["userCode"]);
                        objUser.userName = Convert.ToString(reader["userName"]);
                        objUser.name = Convert.ToString(reader["name"]);
                        objUser.lastName = Convert.ToString(reader["lastName"]);
                        objUser.profileId = Convert.ToInt32(reader["profileId"]);
                        objUser.dni = Convert.ToInt32(reader["dni"]);
                        objUser.email = Convert.ToString(reader["email"]);
                        objUser.state = Convert.ToInt32(reader["state"]);
                        objUser.flagLocked = Convert.ToInt32(reader["isLocked"]);

                    }
                }
                reader.Close();

                BIOMConexion.CloseConexion();

                return objUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /*
        public User GetUserByProcessBussinessCode(string processBussinessCode)
        {
            try
            {
                User objUser = null;

                string strsql = "usp_GetUserByUserBussinessCode";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userBussinessCode", System.Data.SqlDbType.VarChar, 10, userBussinessCode));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objUser = new User();
                        objUser.userId = Convert.ToInt32(reader["userId"]);
                        objUser.userCode = Convert.ToString(reader["userCode"]);
                        objUser.userName = Convert.ToString(reader["userName"]);
                        objUser.name = Convert.ToString(reader["name"]);
                        objUser.lastName = Convert.ToString(reader["lastName"]);
                        objUser.profileId = Convert.ToInt32(reader["profileId"]);
                        objUser.dni = Convert.ToInt32(reader["dni"]);
                        //objUser.enrollId = Convert.ToInt32(reader["enrollId"]);
                        objUser.email = Convert.ToString(reader["email"]);
                        objUser.state = Convert.ToInt32(reader["state"]);
                        objUser.flagLocked = Convert.ToInt32(reader["isLocked"]);

                    }
                }
                reader.Close();

                BIOMConexion.CloseConexion();

                return objUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        */

        /*

        public List<UserQuery> getUserByProfileCustomer(int CustomerId, string profileIds)
        {
            try
            {
                List<UserQuery> resul = new List<UserQuery>();

                string strsql = "usp_getUserByProfileCustomer";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@CustomerId", System.Data.SqlDbType.Int, CustomerId));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@ProfileIds", System.Data.SqlDbType.VarChar, 8000, profileIds));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        UserQuery obj = new UserQuery();
                        obj.userCode = Convert.ToString(reader["userCode"]);
                        obj.fcmToken = Convert.ToString(reader["fcmToken"]);
                        obj.name = Convert.ToString(reader["name"]);
                        obj.lastName = Convert.ToString(reader["lastName"]);
                        obj.email = Convert.ToString(reader["email"]);


                        resul.Add(obj);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        */
        public bool CreateUser(ref User objUser)
        {
            try
            {
                if (objUser.userCode == null)
                {
                    string strsql = "usp_CreateUser";
                    List<SqlParameter> parametros = new List<SqlParameter>();
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userName", System.Data.SqlDbType.VarChar, 50, objUser.userName));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userBussinessCode", System.Data.SqlDbType.VarChar, 10, objUser.userBussinessCode));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@password", System.Data.SqlDbType.VarChar, 100, objUser.password));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@name", System.Data.SqlDbType.VarChar, 50, objUser.name));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@lastName", System.Data.SqlDbType.VarChar, 50, objUser.lastName));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@email", System.Data.SqlDbType.VarChar, 128, objUser.email));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@dni", System.Data.SqlDbType.Int, objUser.dni));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@emisorId", System.Data.SqlDbType.Int, objUser.emisorId));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@profileId", System.Data.SqlDbType.Int, objUser.profileId));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@createdUser", System.Data.SqlDbType.Int, objUser.createdUser));
                    BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                }
                else
                {
                    string strsql = "usp_UpdateUser";
                    List<SqlParameter> parametros = new List<SqlParameter>();
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userCode", System.Data.SqlDbType.VarChar, 100, objUser.userCode));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@name", System.Data.SqlDbType.VarChar, 50, objUser.name));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@lastName", System.Data.SqlDbType.VarChar, 50, objUser.lastName));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@dni", System.Data.SqlDbType.Int, objUser.dni));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@email", System.Data.SqlDbType.VarChar, 128, objUser.email));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@profileId", System.Data.SqlDbType.Int, objUser.profileId));
                    BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int doValidateUserExistsCreate(User objUser)
        {
            try
            {
                int codeResult = 0;
                string strsql = "usp_doValidateUserExistsCreate";
                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter parametroSalida = BIOMConexion.GetUtilBD().CreateParameter("@codeResult", System.Data.SqlDbType.Int, codeResult);
                parametroSalida.Direction = ParameterDirection.InputOutput;

                parametros.Add(parametroSalida);
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userBussinessCode", System.Data.SqlDbType.VarChar, 10, objUser.userBussinessCode));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userName", System.Data.SqlDbType.VarChar, 50, objUser.userName));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@dni", System.Data.SqlDbType.Int, objUser.dni));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@email", System.Data.SqlDbType.VarChar, 128, objUser.email));

                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                codeResult = Convert.ToInt32(parametroSalida.Value);

                return codeResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int doValidateUserExistsEdit(User objUser)
        {
            try
            {
                int codeResult = 0;
                string strsql = "usp_doValidateUserExistsEdit";
                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter parametroSalida = BIOMConexion.GetUtilBD().CreateParameter("@codeResult", System.Data.SqlDbType.Int, codeResult);
                parametroSalida.Direction = ParameterDirection.InputOutput;

                parametros.Add(parametroSalida);
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userCode", System.Data.SqlDbType.VarChar, 100, objUser.userCode));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userBussinessCode", System.Data.SqlDbType.VarChar, 10, objUser.userBussinessCode));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userName", System.Data.SqlDbType.VarChar, 50, objUser.userName));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@dni", System.Data.SqlDbType.Int, objUser.dni));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@email", System.Data.SqlDbType.VarChar, 128, objUser.email));

                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                codeResult = Convert.ToInt32(parametroSalida.Value);

                return codeResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }       

        public User Get(string userCode)
        {
            try
            {
                User resul = null;

                string strsql = "usp_GetUser";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userCode", System.Data.SqlDbType.VarChar, 100, userCode));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        resul = new User();

                        resul.userId = Convert.ToInt32(reader["userId"]);
                        resul.userCode = Convert.ToString(reader["userCode"]);
                        resul.userBussinessCode = Convert.ToString(reader["userBussinessCode"]);
                        resul.userName = Convert.ToString(reader["userName"]);
                        resul.password = Convert.ToString(reader["password"]);
                        resul.name = Convert.ToString(reader["name"]);
                        resul.lastName = Convert.ToString(reader["lastName"]);
                        resul.profileId = Convert.ToInt32(reader["profileId"]);
                        resul.dni = Convert.ToInt32(reader["dni"]);
                        resul.email = Convert.ToString(reader["email"]);
                        resul.emisorId = Convert.ToInt32(reader["emisorId"]);
                        resul.state = Convert.ToInt32(reader["state"]);
                        resul.flagLocked = Convert.ToInt32(reader["isLocked"]);

                        break;
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public bool UpdateUser(ref User objUser)
        //{
        //    try
        //    {
        //        string strsql = "usp_UpdateUser";
        //        List<SqlParameter> parametros = new List<SqlParameter>();
        //        parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userCode", System.Data.SqlDbType.VarChar, 100, objUser.userCode));
        //        parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@name", System.Data.SqlDbType.VarChar, 50, objUser.name));
        //        parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@lastName", System.Data.SqlDbType.VarChar, 50, objUser.lastName));               
        //        parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@customerId", System.Data.SqlDbType.Int, objUser.customerId));              
        //        BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public bool DeleteUser(ref User objUser)
        {
            try
            {
                string strsql = "usp_DeleteUser";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userCode", System.Data.SqlDbType.VarChar, 100, objUser.userCode));
                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool LockedUser(ref User objUser)
        {
            try
            {
                string strsql = "usp_LockedUser";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userCode", System.Data.SqlDbType.VarChar, 100, objUser.userCode));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@flag", System.Data.SqlDbType.Int, objUser.flagLocked));
                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ResetPassword(ref User objUser)
        {
            try
            {
                string strsql = "usp_ResetPassword";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userCode", System.Data.SqlDbType.VarChar, 100, objUser.userCode));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@newPassword", System.Data.SqlDbType.VarChar, 100, objUser.newPassword));
                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
