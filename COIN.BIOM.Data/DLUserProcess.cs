﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COIN.BIOM.Entity;
using System.Data.SqlClient;

namespace COIN.BIOM.Data
{
    public class DLUserProcess
    {

        private BIOMConexion BIOMConexion = new BIOMConexion(); 

        public bool IncludeUserToProcess(ref UserProcess objUserProcess)
        {
            try
            {
                    string strsql = "usp_IncludeUserToProcess";
                    List<SqlParameter> parametros = new List<SqlParameter>();
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userId", System.Data.SqlDbType.Int, objUserProcess.userId));
                    parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processId", System.Data.SqlDbType.Int, objUserProcess.processId));
                    BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);
              
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool doValidateUserProcessExists(UserProcess objUserProcess)
        {
            try
            {
                bool resul = false;

                string strsql = "usp_doValidateUserProcessExists";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userId", System.Data.SqlDbType.Int, objUserProcess.userId));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processId", System.Data.SqlDbType.Int, objUserProcess.processId));

                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        resul = true;
                        break;
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ExcludeUserFromProcess(ref UserProcess objUserProcess)
        {
            try
            {
                string strsql = "usp_DeleteUserProcess";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userId", System.Data.SqlDbType.Int, objUserProcess.userId));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processId", System.Data.SqlDbType.Int, objUserProcess.processId));
                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}
