﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COIN.BIOM.Entity.query;
using COIN.BIOM.Entity;
using System.Data.SqlClient;

namespace COIN.BIOM.Data
{
    public class DLSale
    {
        private BIOMConexion BIOMConexion = new BIOMConexion();


        
        public List<SaleQuery> GetAllSales()
        {
            try
            {
                List<SaleQuery> resul = new List<SaleQuery>();

                string strsql = "usp_GetAllSales";
                List<SqlParameter> parametros = new List<SqlParameter>();
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, null);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        SaleQuery obj = new SaleQuery();

                        obj.emisorId = Convert.ToInt32(reader["emisorId"]);
                        obj.saleId = Convert.ToInt32(reader["saleId"]);
                        obj.paymentType = Convert.ToString(reader["paymentType"]);
                        obj.paymentAmount = Convert.ToDecimal(reader["paymentAmount"]);
                        obj.createdDate = Convert.ToDateTime(reader["createdDate"]);
                        obj.completeName = Convert.ToString(reader["collector"]);
                        obj.attentionOrderDetailId = Convert.ToInt32(reader["attentionOrderDetailId"]);
                        obj.productBussinessCode = Convert.ToString(reader["productBussinessCode"]);
                        obj.description = Convert.ToString(reader["description"]);
                        obj.unitValue = Convert.ToDecimal(reader["unitValue"]);
                        obj.quantity = Convert.ToInt32(reader["quantity"]);
                        obj.subTotal = Convert.ToDecimal(reader["subTotal"]);

                        resul.Add(obj);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        







    }
}
