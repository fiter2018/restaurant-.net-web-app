﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.query;

namespace COIN.BIOM.Data
{
    public class DLProcess
    {
        private BIOMConexion BIOMConexion = new BIOMConexion();

        public List<ProcessList> List(string code, string name, int applicationId)
        {
            try
            {
                List<ProcessList> resul = new List<ProcessList>();

                string strsql = "usp_GetProcessByParams";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processBussinessCode", System.Data.SqlDbType.VarChar, 50, code));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processName", System.Data.SqlDbType.VarChar, 100, name));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@applicationId", System.Data.SqlDbType.Int, applicationId));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ProcessList obj = new ProcessList();

                        obj.processId = Convert.ToInt32(reader["processId"]);
                        obj.processCode = Convert.ToString(reader["processCode"]);
                        obj.processBussinessCode = Convert.ToString(reader["processBussinessCode"]);
                        obj.processName = Convert.ToString(reader["processName"]);
                        obj.processDescription = Convert.ToString(reader["processDescription"]);
                        obj.applicationId = Convert.ToInt32(reader["applicationId"]);
                        obj.applicationName = Convert.ToString(reader["applicationName"]);
                        obj.state = Convert.ToInt32(reader["state"]);
                        obj.createdUser = Convert.ToInt32(reader["createdUser"]);
                        obj.createdDate = Convert.ToDateTime(reader["createdDate"]);

                        resul.Add(obj);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProcessUsersQuery> ListProcessUsersValids(string code, string name, int applicationId)
        {
            try
            {
                List<ProcessUsersQuery> resul = new List<ProcessUsersQuery>();

                string strsql = "usp_GetUsersByProcessValids";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processBussinessCode", System.Data.SqlDbType.VarChar, 10, code));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processName", System.Data.SqlDbType.VarChar, 100, name));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@applicationId", System.Data.SqlDbType.Int, applicationId));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ProcessUsersQuery obj = new ProcessUsersQuery();

                        obj.processCode = Convert.ToString(reader["processCode"]);
                        obj.processBussinessCode = Convert.ToString(reader["processBussinessCode"]);
                        obj.processName = Convert.ToString(reader["processName"]);
                        obj.processDescription = Convert.ToString(reader["processDescription"]);
                        obj.applicationBussinessCode = Convert.ToString(reader["applicationBussinessCode"]);
                        obj.applicationName = Convert.ToString(reader["applicationName"]);
                        obj.userCode = reader["userCode"] == DBNull.Value ? "--" : Convert.ToString(reader["userCode"]);
                        obj.userBussinessCode = reader["userBussinessCode"] == DBNull.Value ? "--" : Convert.ToString(reader["userBussinessCode"]);
                        obj.completeNameUser = reader["completeName"] == DBNull.Value ? "--" : Convert.ToString(reader["completeName"]);
                        obj.userName = reader["userName"] == DBNull.Value ? "--" : Convert.ToString(reader["userName"]);
                        obj.dni = reader["dni"] == DBNull.Value ? 0 : Convert.ToInt32(reader["dni"]);

                        String profileName = "--";
                        if (reader["profileId"] != DBNull.Value)
                        {
                            switch (Convert.ToInt32(reader["profileId"]))
                            {
                                case 1: profileName = "Administrador"; break;
                                case 2: profileName = "Responsable Sucursal"; break;
                                case 3: profileName = "Usuario"; break;
                            }
                        }

                        obj.profileName = profileName;
                        obj.email = reader["email"] == DBNull.Value ? "--" : Convert.ToString(reader["email"]);

                        if (reader["assignmentDate"] != DBNull.Value)
                        {
                            obj.assignmentDate = Convert.ToDateTime(reader["assignmentDate"]);
                        }

                        resul.Add(obj);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ProcessUsersQuery> ListProcessUsers(string code, string name, int applicationId)
        {
            try
            {
                List<ProcessUsersQuery> resul = new List<ProcessUsersQuery>();

                string strsql = "usp_GetUsersByProcess";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processBussinessCode", System.Data.SqlDbType.VarChar, 50, code));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processName", System.Data.SqlDbType.VarChar, 100, name));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@applicationId", System.Data.SqlDbType.Int, applicationId));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        ProcessUsersQuery obj = new ProcessUsersQuery();

                        obj.processCode = Convert.ToString(reader["processCode"]);
                        obj.processBussinessCode = Convert.ToString(reader["processBussinessCode"]);
                        obj.processName = Convert.ToString(reader["processName"]);
                        obj.processDescription = Convert.ToString(reader["processDescription"]);
                        obj.applicationBussinessCode = Convert.ToString(reader["applicationBussinessCode"]);
                        obj.applicationName = Convert.ToString(reader["applicationName"]);
                        obj.userCode = reader["userCode"] == DBNull.Value ? "--" : Convert.ToString(reader["userCode"]);
                        obj.userBussinessCode = reader["userBussinessCode"] == DBNull.Value ? "--" : Convert.ToString(reader["userBussinessCode"]);
                        obj.completeNameUser = reader["completeName"] == DBNull.Value ? "--" : Convert.ToString(reader["completeName"]);
                        obj.userName = reader["userName"] == DBNull.Value ? "--" : Convert.ToString(reader["userName"]);
                        obj.dni = reader["dni"] == DBNull.Value ? 0 : Convert.ToInt32(reader["dni"]);

                        String profileName = "--";
                        if(reader["profileId"] != DBNull.Value) {
                            switch (Convert.ToInt32(reader["profileId"])) {
                                case 1: profileName = "Administrador"; break;
                                case 2: profileName = "Responsable Sucursal"; break;
                                case 3: profileName = "Usuario"; break;
                            }
                        }

                        obj.profileName = profileName;
                        obj.email = reader["email"] == DBNull.Value ? "--" : Convert.ToString(reader["email"]);

                        if (reader["assignmentDate"] != DBNull.Value) {
                            obj.assignmentDate = Convert.ToDateTime(reader["assignmentDate"]);
                        }

                        resul.Add(obj);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Process Get(string processCode)
        {
            try
            {
                Process resul = null;

                string strsql = "usp_GetProcessByCode";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processCode", System.Data.SqlDbType.VarChar, 50, processCode));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        resul = new Process();

                        resul.processId = Convert.ToInt32(reader["processId"]);
                        resul.processCode = Convert.ToString(reader["processCode"]);
                        resul.processBussinessCode = Convert.ToString(reader["processBussinessCode"]);
                        resul.processName = Convert.ToString(reader["processName"]);
                        resul.processDescription = Convert.ToString(reader["processDescription"]);
                        resul.applicationId = Convert.ToInt32(reader["applicationId"]);
                        resul.applicationBussinessCode = Convert.ToString(reader["applicationBussinessCode"]);
                        resul.applicationName = Convert.ToString(reader["applicationName"]);
                        resul.fingerprintQuantity = Convert.ToInt32(reader["fingerprintQuantity"]);
                        resul.state = Convert.ToInt32(reader["state"]);
                        resul.createdUser = Convert.ToInt32(reader["createdUser"]);
                        break;
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Process GetByProcessBussinessCode(string processBussinessCode)
        {
            try
            {
                Process resul = null;

                string strsql = "usp_GetProcessByProcessBussinessCode";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processBussinessCode", System.Data.SqlDbType.VarChar, 10, processBussinessCode));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        resul = new Process();

                        resul.processId = Convert.ToInt32(reader["processId"]);
                        resul.processCode = Convert.ToString(reader["processCode"]);
                        resul.processBussinessCode = Convert.ToString(reader["processBussinessCode"]);
                        resul.processName = Convert.ToString(reader["processName"]);
                        resul.processDescription = Convert.ToString(reader["processDescription"]);
                        resul.applicationId = Convert.ToInt32(reader["applicationId"]);
                        resul.applicationBussinessCode = Convert.ToString(reader["applicationBussinessCode"]);
                        resul.applicationName = Convert.ToString(reader["applicationName"]);
                        resul.state = Convert.ToInt32(reader["state"]);
                        resul.createdUser = Convert.ToInt32(reader["createdUser"]);
                        break;
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool Save(ref Process objProcess)
        {
            try
            {
                string strsql = "usp_CreateProcess";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processBussinessCode", System.Data.SqlDbType.VarChar, 10, objProcess.processBussinessCode));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processName", System.Data.SqlDbType.VarChar, 100, objProcess.processName));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@applicationBussinessCode", System.Data.SqlDbType.VarChar, 10, objProcess.applicationBussinessCode));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@fingerprintQuantity", System.Data.SqlDbType.Int, objProcess.fingerprintQuantity));
                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Edit(ref Process objProcess)
        {
            try
            {
                string strsql = "usp_EditProcess";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processCode", System.Data.SqlDbType.VarChar, 100, objProcess.processCode));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processName", System.Data.SqlDbType.VarChar, 100, objProcess.processName));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@fingerprintQuantity", System.Data.SqlDbType.Int, objProcess.fingerprintQuantity));
                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool doValidateProcessExists(Process objProcess)
        {
            try
            {
                bool resul = false;

                string strsql = "usp_doValidateProcessExists";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processBussinessCode", System.Data.SqlDbType.VarChar, 10, objProcess.processBussinessCode));

                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        resul = true;
                        break;
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Delete(string processCode)
        {
            try
            {
                string strsql = "usp_DeleteProcess";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@processCode", System.Data.SqlDbType.VarChar, 100, processCode));
                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Process> GetProcess()
        {
            try
            {
                List<Process> resul = new List<Process>();

                string strsql = "usp_getProcess";
                List<SqlParameter> parametros = new List<SqlParameter>();
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, null);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Process obj = new Process();

                        obj.processId = Convert.ToInt32(reader["processId"]);
                        obj.processName = Convert.ToString(reader["processName"]);

                        resul.Add(obj);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }





    }
}
