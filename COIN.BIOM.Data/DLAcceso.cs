﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using COIN.BIOM.Entity;


namespace COIN.BIOM.Data
{
    public class DLAcceso
    {
        private BIOMConexion BIOMConexion = new BIOMConexion();

        public int getLogin(Login datos)
        {
            try
            {
                int codeResult = 0;
                string strsql = "usp_Login";
                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter parametroSalida = BIOMConexion.GetUtilBD().CreateParameter("@codeResult", System.Data.SqlDbType.Int, codeResult);
                parametroSalida.Direction = ParameterDirection.InputOutput;

                parametros.Add(parametroSalida);
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userName", System.Data.SqlDbType.VarChar, 50, datos.usuario));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@password", System.Data.SqlDbType.VarChar, 100, datos.clave));

                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                codeResult = Convert.ToInt32(parametroSalida.Value);

                return codeResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public int getRecoveryPass(Login objDatos)
        {
            try
            {
                int codeResult = 0;
                string strsql = "usp_RecoveryPass";
                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter parametroSalida = BIOMConexion.GetUtilBD().CreateParameter("@codeResult", System.Data.SqlDbType.Int, codeResult);
                parametroSalida.Direction = ParameterDirection.InputOutput;

                parametros.Add(parametroSalida);
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@email", System.Data.SqlDbType.VarChar, 128, objDatos.email));

                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                codeResult = Convert.ToInt32(parametroSalida.Value);

                return codeResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public User GetUserByName(string userName)
        {
            try
            {
                User objUser = null;

                string strsql = "usp_GetUserByName";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userName", System.Data.SqlDbType.VarChar, 50, userName));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objUser = new User();
                        objUser.userId = Convert.ToInt32(reader["userId"]);
                        objUser.userCode = Convert.ToString(reader["userCode"]);
                        objUser.userName = Convert.ToString(reader["userName"]);
                        objUser.name = Convert.ToString(reader["name"]);
                        objUser.lastName = Convert.ToString(reader["lastName"]);
                        objUser.profileId = Convert.ToInt32(reader["profileId"]);
                        objUser.dni = Convert.ToInt32(reader["dni"]);
                        objUser.email = Convert.ToString(reader["email"]);

                    }
                }
                reader.Close();

                BIOMConexion.CloseConexion();

                return objUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public User GetUserByEmail(string email)
        {
            try
            {
                User objUser = null;

                string strsql = "usp_GetUserByEmail";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@email", System.Data.SqlDbType.VarChar, 128, email));
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        objUser = new User();
                        objUser.userId = Convert.ToInt32(reader["userId"]);
                        objUser.userCode = Convert.ToString(reader["userCode"]);
                        objUser.userName = Convert.ToString(reader["userName"]);
                        objUser.name = Convert.ToString(reader["name"]);
                        objUser.lastName = Convert.ToString(reader["lastName"]);
                        objUser.profileId = Convert.ToInt32(reader["profileId"]);
                        objUser.dni = Convert.ToInt32(reader["dni"]);
                        objUser.email = Convert.ToString(reader["email"]);
                        objUser.state = Convert.ToInt32(reader["state"]);
                        objUser.flagLocked = Convert.ToInt32(reader["isLocked"]);

                    }
                }
                reader.Close();

                BIOMConexion.CloseConexion();

                return objUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CreateToken(int typeToken, int userId)
        {
            try
            {
                string tokenCode = string.Empty;
                string strsql = "usp_CreateToken";

                List<SqlParameter> parametros = new List<SqlParameter>();
                SqlParameter parametroSalida = BIOMConexion.GetUtilBD().CreateParameter("@TokenCode", System.Data.SqlDbType.VarChar, 100, tokenCode);
                parametroSalida.Direction = ParameterDirection.InputOutput;

                parametros.Add(parametroSalida);
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@typeToken", System.Data.SqlDbType.Int, typeToken));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@userId", System.Data.SqlDbType.Int, userId));

                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                tokenCode = Convert.ToString(parametroSalida.Value);

                return tokenCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int getValidateCode(Login datos)
        {
            try
            {
                int codeResult = 0;
                string strsql = "usp_ValidateCode";
                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter parametroSalida = BIOMConexion.GetUtilBD().CreateParameter("@codeResult", System.Data.SqlDbType.Int, codeResult);
                parametroSalida.Direction = ParameterDirection.InputOutput;

                parametros.Add(parametroSalida);
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@tokenCode", System.Data.SqlDbType.VarChar, 100, datos.token));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@recoveryCode", System.Data.SqlDbType.VarChar, 50, datos.recoveryCode));

                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                codeResult = Convert.ToInt32(parametroSalida.Value);

                return codeResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int getResetPass(Login datos)
        {
            try
            {
                int codeResult = 0;
                string strsql = "usp_ResetPass";
                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter parametroSalida = BIOMConexion.GetUtilBD().CreateParameter("@codeResult", System.Data.SqlDbType.Int, codeResult);
                parametroSalida.Direction = ParameterDirection.InputOutput;

                parametros.Add(parametroSalida);
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@tokenCode", System.Data.SqlDbType.VarChar, 100, datos.token));
                parametros.Add(BIOMConexion.GetUtilBD().CreateParameter("@newPassword", System.Data.SqlDbType.VarChar, 100, datos.nuevaClave));

                BIOMConexion.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                codeResult = Convert.ToInt32(parametroSalida.Value);

                return codeResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }






    }
}
