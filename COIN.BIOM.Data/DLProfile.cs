﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using COIN.BIOM.Entity;
using System.Data.SqlClient;


namespace COIN.BIOM.Data
{
    public class DLProfile
    {
        private BIOMConexion BIOMConexion = new BIOMConexion();

        public List<Profile> GetProfile()
        {
            try
            {
                List<Profile> resul = new List<Profile>();

                string strsql = "usp_GetProfile";
                List<SqlParameter> parametros = new List<SqlParameter>();
                SqlDataReader reader = BIOMConexion.GetUtilBD().GetReader(strsql, null);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Profile obj = new Profile();

                        obj.profileId = Convert.ToInt32(reader["profileId"]);
                        obj.name = Convert.ToString(reader["name"]);

                        resul.Add(obj);
                    }
                }
                reader.Close();
                BIOMConexion.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
