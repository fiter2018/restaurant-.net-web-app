﻿using System;
using System.IO;
using log4net;
using log4net.Config;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace COIN.BIOM.Logger
{
    public static class LogManagerWrapper
    {
        private static readonly string LOG_CONFIG_FILE = ConfigurationManager.AppSettings["RUTALOGXML"];

        public static ILog GetLogger(Type type)
        {
            // If no loggers have been created, load our own.
            if (LogManager.GetCurrentLoggers().Length == 0)
            {
                LoadConfig();
            }
            return LogManager.GetLogger(type);
        }

        private static void LoadConfig()
        {
            //// TODO: Do exception handling for File access issues and supply sane defaults if it's unavailable.   
            XmlConfigurator.ConfigureAndWatch(new FileInfo(LOG_CONFIG_FILE));
        }
    }
}
