﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using COIN.BIOM.Entity;

namespace COIN.BIOM.Web.Models
{
    public class UserProcessModel
    {
        public int userId { get; set; }
        public string userCode { get; set; }
        public string userBussinessCode { get; set; }
        public string profileName { get; set; }
        public byte[] FingerTemplate { get; set; }
        //public string flgPasswordChange { get; set; }
        
        public string userName { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string lastName { get; set; }
        public string completeName { get; set; }
        public int dni { get; set; }
        public int enrollId { get; set; }
        public int profileId { get; set; }
        public int userCreatedUser { get; set; }
        public string email { get; set; }

        public string newPassword { get; set; }
        public string confirmPassword { get; set; }

        public int userState { get; set; }
        public int flagLocked { get; set; }

        public bool viewNotificationFlag { get; set; }
        public int repeatingTime { get; set; }
        

        public int processId { get; set; }
        public string processCode { get; set; }
        public string processBussinessCode { get; set; }
        public string processName { get; set; }
        public string processDescription { get; set; }
        public int applicationId { get; set; }
        public string applicationBussinessCode { get; set; }
        public string applicationName { get; set; }
        public int processFingerprintQuantity { get; set; }
        public int processState { get; set; }
        public int processCreatedUser { get; set; }


        public DateTime? assignmentDate { get; set; }
        public int userProcessState { get; set; }

    }
}