﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace COIN.BIOM.Web.Models
{
    public class LoginModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string token { get; set; }
        public string recoveryCode { get; set; }
        public string nuevaClave { get; set; }
        public string confirmaClave { get; set; }
    }
}