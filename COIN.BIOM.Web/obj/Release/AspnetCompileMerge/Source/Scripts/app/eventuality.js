﻿var flagEnviar = 0;

$(function () {
    $("#frmBusqueda").on("submit", function (e) {
        if (flagEnviar == 1) {
            flagEnviar = 0;
        }
        else {
            e.preventDefault();
        }
    });

    $("#btnExportar").on("click", function (e) {
        flagEnviar = 1;
        $('#hdnFechaDesde').val($('#txtFechaDesde').val());
        $('#hdnFechaHasta').val($('#txtFechaHasta').val());
        $("#frmBusqueda").submit();
    });

});

