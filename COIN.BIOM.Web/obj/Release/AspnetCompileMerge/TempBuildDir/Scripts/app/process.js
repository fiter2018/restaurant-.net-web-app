﻿var flagEnviar = 0;

$(function () {

    $("#frmBusqueda").on("submit", function (e) {
        if (flagEnviar) {
            flagEnviar = 0;
        } else {
            e.preventDefault();
        }
    });

    $("#tabPrincipal").DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        columns: [
        { data: null },
        { data: null },
        { data: null },
        { data: "processCode" },
        { data: "processBussinessCode" },
        { data: "processName" },
        { data: "applicationName" },
        ],
        columnDefs: [
            {
                "targets": [3],
                "visible": false
            },
            {
                "targets": 0,
                "orderable": false,
                "data": "processCode",
                "render": function (data, type, full, meta) {
                    return "<a href='" + fnBaseUrlWeb("Process/Edit?processCode=" + data.processCode) + "' title='Editar' /><i class='fa fa-fw fa-edit'></i></a>";
                }
            },
            {
                "targets": 1,
                "orderable": false,
                "data": "processCode",
                "render": function (data, type, full, meta) {
                    return "<a href='javascript:fnEliminar(\"" + data.processCode + "\");' title='Eliminar' /><i class='fa fa-fw fa-remove'></i></a>";
                }
            },
            {
                "targets": 2,
                "orderable": false,
                "data": "processCode",
                "render": function (data, type, full, meta) {
                    return "<a href='javascript:fnAgregarUsuario(\"" + data.processCode + "\");' title='Agregar Usuario' /><i class='fa fa-fw fa-user-plus'></i></a>";
                }
            }]
    });


    $("#btnBuscar").on("click", function (e) {
        fnRefrescar();
    });

    
    $("#btnNuevo").on("click", function (e) {
        //fnLimpiarControles();
        //$("#divPrincipal").modal("show");

    });

    $("#btnExportar").on("click", function (e) {
        flagEnviar = 1;
        $("#frmBusqueda").submit();
        //fnExportar();
    });
    
    $("#btnGuardar").on("click", function (e) {
        fnGuardar();
    });

    $("#frmPrincipal").on("submit", function (e) {
        e.preventDefault();
    });

    fnRefrescar();

});


function fnRefrescar() {
    var params = new Object();
    params.code = $("#txtProcessBussinessCode").val().length === 0 ? "" : $("#txtProcessBussinessCode").val();
    params.name = $("#txtProcessName").val().length === 0 ? "" : $("#txtProcessName").val();

    if ($("#ddlAplicacion").val() === "" || $("#ddlAplicacion").val() === null) {
        params.applicationId = 0;
    } else {
        var aplicacionInt = parseInt($("#ddlAplicacion").val());
        if (isNaN(aplicacionInt)) {
            params.applicationId = 0;
        } else {
            params.applicationId = aplicacionInt;
        }
    }

    Get("Process/List", params).done(function (response) {
        if (response.result == 0) {
            fnLimpiarTabla($('#tabPrincipal').dataTable());
            if (response.data.length > 0) {
                $('#tabPrincipal').dataTable().fnAddData(response.data);
            }
        }
    });
}

function fnExportar() {
    var params = new Object();
    params.code = $("#txtProcessBussinessCode").val().length === 0 ? "" : $("#txtProcessBussinessCode").val();
    params.name = $("#txtProcessName").val().length === 0 ? "" : $("#txtProcessName").val();

    if ($("#ddlAplicacion").val() === "" || $("#ddlAplicacion").val() === null) {
        params.applicationId = 0;
    } else {
        var aplicacionInt = parseInt($("#ddlAplicacion").val());
        if (isNaN(aplicacionInt)) {
            params.applicationId = 0;
        } else {
            params.applicationId = aplicacionInt;
        }
    }

    Get("Process/DownloadListProcessUsers", params).done(function (response) {

        fnAlertar("Información", "Documento exportado en carpeta 'Temporal'", function () {
            fnRefrescar();
        });

    });
}

function fnGuardar() {

    //Guardando usuario en tabla usuario
    var params = $("#frmPrincipal").serialize();

    $("#frmPrincipal").validate({
        rules: {
            "userBussinessCode": {
                required: true               
            },
            "email": {
                required: true,
                email: true
            },
            "dni": {
                required: true,
                minlength: 8
            },
            "userName": {
                required: true,
                minlength: 6
            },
            "name": {
                required: true
            },
            "lastName": {
                required: true
            },
            "password": {
                required: true,
                minlength: 6
            },
            "confirmPassword": {
                required: true,
                minlength: 6,
                equalTo: "#txtPasswordMantUser"
            },
        }
    });    
    
    if ($("#frmPrincipal").valid()) {
        Post("UserProcess/AddNewUserAndAssignToProcess", params).done(function (response) {
            if (response.result == 0) {
                fnAlertar("Información", response.message, function () {
                    $("#divPrincipal").modal("hide");
                });
            } else {
                fnAlertar("Alerta", response.message);
            }
        });
    }
    
    //Guardando usuario-proceso en tabla UserProcess


}


//id == processCode
function fnEditar(id) {

    Get("Process/Nuevo/" + id).done(function (response) {
        if (response.result == 0) {
            $("#hdnCustomerCode").val(id);
            $("#customer_code").val(response.data.code);
            $("#customer_name").val(response.data.name);
            $("#customer_ruc").val(response.data.ruc);

            $("#customer_code").attr("readonly", "readonly");


            $("#divPrincipal").modal("show");
        }
    });
}
//id == processCode
function fnEliminar(id) {
    var params = new Object();
    params.Id = id;

    fnConfirmar("Eliminar", "¿Seguro que desea eliminar?", function () {
        Post("Process/Delete", params).done(function (response) {
            if (response.result == 0) {
                fnRefrescar();
            }
        });
    });
}
//id == processCode
function fnAgregarUsuario(id) {
    fnLimpiarControles();
    $("#hdnProcessCode").val(id);
    $("#hdnUserCode").val("");
    Get("Process/Get/" + id).done(function (response) {
        if (response.result == 0) {
            $("#txtProcessNameMantUser").val(response.data.processName);
            $("#txtProcessNameMantUser").attr("readonly", "readonly");
        }
    });
    $("#divPrincipal").modal("show");
}


function fnLimpiarControles() {
    $("#txtUserNameMantUser").val("");
    $("#txtPasswordMantUser").val("");
    $("#txtConfirmPasswordMantUser").val("");
    $("#txtNameMantUser").val("");
    $("#txtLastNameMantUser").val("");
    $("#txtEmailMantUser").val("");
    $("#txtDniMantUser").val("");
    $("#ddlPerfilMantUser").val("");

}
