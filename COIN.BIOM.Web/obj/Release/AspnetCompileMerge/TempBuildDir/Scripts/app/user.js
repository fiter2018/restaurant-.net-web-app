﻿
var flagEnviar = 0;

$(function () {

    $("#frmBusqueda").on("submit", function (e) {
        if(flagEnviar){
            flagEnviar = 0;
        } else {
            e.preventDefault();
        }
    });

    $("#tabPrincipal").DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        columns: [
        { data: null },
        { data: null },
        { data: null },
        { data: null },
        { data: "userCode" },
        { data: "flagLocked" },
        { data: "userBussinessCode" },
        { data: "name" },
        { data: "lastName" },
        { data: "userName" },
        { data: "profileName" },
        { data: "dni" },
        { data: "email" },
        ],
        columnDefs: [
            {
                "targets": [4, 5],
                "visible": false
            },
            {
                "targets": 0,
                "orderable": false,
                "data": "userCode",
                "render": function (data, type, full, meta) {
                    return "<a href='javascript:fnEditar(\"" + data.userCode + "\");' title='Editar' /><i class='fa fa-fw fa-edit'></i></a>";
                }
            },
            {
                "targets": 1,
                "orderable": false,
                "data": "userCode",
                "render": function (data, type, full, meta) {
                    return "<a href='javascript:fnEliminar(\"" + data.userCode + "\");' title='Eliminar' /><i class='fa fa-fw fa-remove'></i></a>";
                }
            },
            {
                "targets": 2,
                "orderable": false,
                "data": "userCode",
                "render": function (data, type, full, meta) {
                    if (full.flagLocked == 0) {
                        return "<a href='javascript:fnBloquear(\"" + data.userCode + "\"," + full.flagLocked + ");' title='Bloquear' /><i class='fa fa-lock'></i></a>";
                    }
                    else {
                        return "<a href='javascript:fnBloquear(\"" + data.userCode + "\"," + full.flagLocked + ");' title='Desbloquear' /><i class='fa fa-unlock-alt'></i></a>";
                    }
                }
            },
            {
                "targets": 3,
                "orderable": false,
                "data": "userCode",
                "render": function (data, type, full, meta) {
                    return "<a href='javascript:fnEditarReset(\"" + data.userCode + "\");' title='Resetear' /><i class='fa fa-key'></i></a>";
                }
            }]
    });


    $("#btnBuscar").on("click", function (e) {
        fnRefrescar();
    });

    $("#btnNuevo").on("click", function (e) {
        fnLimpiarControles();
        $("#divPrincipal").modal("show");
    });

    $("#btnExportar").on("click", function (e) {
        flagEnviar = 1;
        $("#frmBusqueda").submit();
        //fnExportar();
    });

    $("#btnGuardar").on("click", function (e) {
        fnGuardar();
    });

    $("#btnReset").on("click", function (e) {
        fnResetear();
    });

    $("#frmPrincipal").on("submit", function (e) {
        e.preventDefault();
    });

    fnRefrescar();

});
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function fnRefrescar() {
    var params = new Object();
    params.CodUsuario = $("#txtUserCodeUserIndex").val().length === 0 ? "" : $("#txtUserCodeUserIndex").val();
    params.NombUsuario = $("#txtUserNameUserIndex").val().length === 0 ? "" : $("#txtUserNameUserIndex").val();

    if ($("#txtDniUserIndex").val() === "" || $("#txtDniUserIndex").val() === null) {
        params.Dni = 0;    
    } else {
        var dniInt = parseInt($("#txtDniUserIndex").val());
        if(isNaN(dniInt)){
            params.Dni = 0;    
        } else {
            params.Dni = dniInt;
        }    
    }

    if ($("#ddlPerfil").val() === "" || $("#ddlPerfil").val() === null) {
        params.Perfil = 0;
    } else {
        var perfilInt = parseInt($("#ddlPerfil").val());
        if (isNaN(perfilInt)) {
            params.Perfil = 0;
        } else {
            params.Perfil = perfilInt;
        }
    }
    
    Get("User/ListUser", params).done(function (response) {
        if (response.result == 0) {
            fnLimpiarTabla($('#tabPrincipal').dataTable());
            if (response.data.length > 0) {
                $('#tabPrincipal').dataTable().fnAddData(response.data);
            }
        }
    });
    
}

function fnExportar() {
    var params = new Object();
    params.CodUsuario = $("#txtUserCodeUserIndex").val().length === 0 ? "" : $("#txtUserCodeUserIndex").val();
    params.NombUsuario = $("#txtUserNameUserIndex").val().length === 0 ? "" : $("#txtUserNameUserIndex").val();

    if ($("#txtDniUserIndex").val() === "" || $("#txtDniUserIndex").val() === null) {
        params.Dni = 0;
    } else {
        var dniInt = parseInt($("#txtDniUserIndex").val());
        if (isNaN(dniInt)) {
            params.Dni = 0;
        } else {
            params.Dni = dniInt;
        }
    }

    if ($("#ddlPerfil").val() === "" || $("#ddlPerfil").val() === null) {
        params.Perfil = 0;
    } else {
        var perfilInt = parseInt($("#ddlPerfil").val());
        if (isNaN(perfilInt)) {
            params.Perfil = 0;
        } else {
            params.Perfil = perfilInt;
        }
    }

    Get("User/DownloadListUser", params).done(function (response) {

        fnAlertar("Información", "Documento exportado en carpeta 'Temporal'", function () {
            fnRefrescar();
        });

    });

}

function fnGuardar() {
    var params = $("#frmPrincipal").serialize();

    $("#frmPrincipal").validate({
        rules: {
            "user.userBussinessCode": {
                required: true                
            },
            "user.email": {
                required: true,
                email: true
            },
            "user.dni": {
                required: true,
                minlength: 8
            },
            "user.userName": {
                required: true,
                minlength: 6
            },
            "user.name": {
                required: true
            },
            "user.lastName": {
                required: true
            },
            "user.password": {
                required: true,
                minlength: 6                
            },
            "user.confirmPassword": {
                required: true,
                minlength: 6,
                equalTo: "#txtPasswordMantUser"
            },
        }
    });

    if ($("#frmPrincipal").valid()) {
        Post("User/Save", params).done(function (response) {
            if (response.result == 0) {
                fnAlertar("Información", response.message, function () {
                    $("#divPrincipal").modal("hide");
                    fnRefrescar();
                });
            } else {
                fnAlertar("Alerta", response.message);
            }
        });
    }
}

function fnEditar(id) {

    Get("User/Get/" + id).done(function (response) {
        if (response.result == 0) {
            $("#hdnUserCode").val(id);
            $("#txtUserBussinessCodeMantUser").val(response.data.userBussinessCode);
            $("#txtUserNameMantUser").val(response.data.userName);
            $("#txtPasswordMantUser").val(response.data.password);
            $("#txtConfirmPasswordMantUser").val(response.data.password);
            $("#txtNameMantUser").val(response.data.name);
            $("#txtLastNameMantUser").val(response.data.lastName);
            $("#txtEmailMantUser").val(response.data.email);
            $("#txtDniMantUser").val(response.data.dni);
            $("#ddlPerfilMantUser").val(response.data.profileId);

            $("#txtUserBussinessCodeMantUser").attr("disabled", "disabled");
            $("#txtUserNameMantUser").attr("readonly", "readonly");
            $("#txtPasswordMantUser").attr("disabled", "disabled");
            $("#txtConfirmPasswordMantUser").attr("disabled", "disabled");

            $("#divPrincipal").modal("show");
        }
    });
}

function fnEditarReset(id) {

    $("#hdnUserCodeReset").val(id);
    $("#divPrincipal3").modal("show");

    Get("User/Get/" + id).done(function (response) {
        if (response.result == 0) {
            $("#hdnUserCodeReset").val(id);
            $("#divPrincipal3").modal("show");
        }
    });
}

function fnResetear(id) {
    var params = $("#frmPrincipal3").serialize();

    $("#frmPrincipal3").validate();

    if ($("#frmPrincipal3").valid()) {
        Post("User/Reset", params).done(function (response) {
            if (response.result == 0) {
                fnAlertar("Información", response.message, function () {
                    $("#divPrincipal3").modal("hide");
                    $("#txtNewPassword").val("");
                    $("#txtConfirmPassword").val("");
                    fnRefrescar();
                });
            } else {
                fnAlertar("Alerta", response.message);
            }
        });
    }
}

function fnEliminar(id) {
    var params = new Object();
    params.id = id;

    fnConfirmar("Eliminar", "¿Seguro que desea eliminar?", function () {
        Post("User/Delete", params).done(function (response) {
            if (response.result == 0) {
                fnRefrescar();
            }
        });
    });
}

function fnBloquear(id, flag) {
    var params = new Object();
    var message1 = "";
    var message2 = "";

    params.id = id;

    if (flag == 0) {
        params.isLocked = 1;
        message1 = "Bloquear";
        message2 = "¿Seguro que desea Bloquear?";
    }
    else {
        params.isLocked = 0;
        message1 = "Desbloquear";
        message2 = "¿Seguro que desea Desbloquear?";
    }

    fnConfirmar(message1, message2, function () {
        Post("User/Locked", params).done(function (response) {
            if (response.result == 0) {
                fnRefrescar();
            }
        });
    });

}

function fnLimpiarControles() {
    $("#hdnUserCode").val("");
    $("#txtUserBussinessCodeMantUser").val("");
    $("#txtUserNameMantUser").val("");
    $("#txtPasswordMantUser").val("");
    $("#txtConfirmPasswordMantUser").val("");
    $("#txtNameMantUser").val("");
    $("#txtLastNameMantUser").val("");
    $("#txtEmailMantUser").val("");
    $("#txtDniMantUser").val("");
    $("#ddlPerfilMantUser").val("");

    $("#txtUserNameMantUser").removeAttr("disabled");
    $("#txtPasswordMantUser").removeAttr("disabled");
    $("#txtConfirmPasswordMantUser").removeAttr("disabled");
    $("#ddlPerfilMantUser").removeAttr("disabled");

}
