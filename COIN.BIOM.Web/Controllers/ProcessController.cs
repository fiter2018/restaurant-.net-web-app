﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using System.IO;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.query;
using COIN.BIOM.Business;
using COIN.BIOM.Web.Models;
using BAZ.GEN.Log4Dan;

namespace COIN.BIOM.Web.Controllers
{
    [Authorize]
    public class ProcessController : Controller
    {

        private Log Log = new Log(typeof(ProcessController));
        private BLProcess oBLProcess;

        public ProcessController()
        {
            oBLProcess = new BLProcess();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(string code = "", string name = "", int applicationId = 0)
        {
            List<ProcessList> lista = oBLProcess.List(code, name, applicationId);

            return Json(new { result = Enums.eCodeError.OK, data = lista }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListProcessUsersValids(string code = "", string name = "", int applicationId = 0)
        {
            List<ProcessUsersQuery> lista = oBLProcess.ListProcessUsersValids(code, name, applicationId);

            return Json(new { result = Enums.eCodeError.OK, data = lista }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ListProcessUsers(string code = "", string name = "", int applicationId = 0)
        {
            List<ProcessUsersQuery> lista = oBLProcess.ListProcessUsers(code, name, applicationId);

            return Json(new { result = Enums.eCodeError.OK, data = lista }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadListProcessUsers(string code = "", string name = "", int applicationId = 0)
        {
            try
            {
                string FileName = string.Format("ReportListProcessUsers_{0:ddMMyyyy_hhmm}.xlsx", DateTime.Now);

                string RutaPlantilla = Server.MapPath("~/Templates/TemplateListProcessUsers.xlsx");
                string RutaGuardarComo = Server.MapPath("~/Temporal/" + FileName);

                using (var pck = new OfficeOpenXml.ExcelPackage())
                {
                    using (var stream = System.IO.File.OpenRead(RutaPlantilla))
                    {
                        pck.Load(stream);
                    }

                    List<ProcessUsersQuery> lista = oBLProcess.ListProcessUsers(code, name, applicationId);

                    var ws = pck.Workbook.Worksheets["Data"];
                    int Fila = 2;
                    int Contador = 1;
                    foreach (ProcessUsersQuery itm in lista)
                    {
                        //Write process detail
                        ws.Cells[Fila, 1].Value = itm.processBussinessCode;
                        ws.Cells[Fila, 2].Value = itm.processName;
                        ws.Cells[Fila, 3].Value = itm.processDescription;
                        ws.Cells[Fila, 4].Value = itm.applicationBussinessCode;
                        ws.Cells[Fila, 5].Value = itm.applicationName;
                        ws.Cells[Fila, 6].Value = Contador;
                        ws.Cells[Fila, 7].Value = itm.userBussinessCode;
                        ws.Cells[Fila, 8].Value = itm.completeNameUser;
                        ws.Cells[Fila, 9].Value = itm.userName;
                        ws.Cells[Fila, 10].Value = itm.dni;
                        ws.Cells[Fila, 11].Value = itm.profileName;
                        ws.Cells[Fila, 12].Value = itm.email;
                        ws.Cells[Fila, 13].Value = string.Format("{0:dd/MM/yyyy HH:mm:ss}", itm.assignmentDate);
                        Fila++;
                        Contador++;
                    }

                    pck.SaveAs(new FileInfo(RutaGuardarComo));
                }

                return File(RutaGuardarComo, System.Net.Mime.MediaTypeNames.Application.Octet, FileName);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                return Content("No hay registros");
            }
        }

        public ActionResult Get(string id)
        {
            ProcessList item = oBLProcess.Get(id);

            return Json(new { result = Enums.eCodeError.OK, data = item }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Save(string processCode = "", string applicationBussinessCode = "", string processBussinessCode = "", string processName = "", int fingerprintQuantity = 0)
        {            
            BLApplication oBLApplication = new BLApplication();

            if (fingerprintQuantity <= 0)
            {
                return Json(new { result = Enums.eCodeError.ERROR, message = "Cantidad de Huellas ingresada es incorrecta." + applicationBussinessCode });
            }

            if (string.IsNullOrEmpty(processCode))
            {
                //Boton Nuevo Proceso --------------------------------------------------------------------------------------------
                //Validamos existencia de aplicación
                Application app = new Application();
                app.applicationBussinessCode = applicationBussinessCode;

                if (!oBLApplication.doValidateApplicationExists(app))
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "No existe Aplicación con código " + applicationBussinessCode});
                }

                //Validamos la no existencia del proceso
                Process process = new Process();
                process.processBussinessCode = processBussinessCode;

                if (oBLProcess.doValidateProcessExists(process))
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un Proceso con el mismo Código" });
                }

                process.processName = processName;
                process.applicationBussinessCode = applicationBussinessCode;

                process.fingerprintQuantity = fingerprintQuantity;
                bool result = oBLProcess.Save(process);

                return Json(new { result = Enums.eCodeError.OK, message = "Guardado Satisfactoriamente" });

            }
            else {
                //Boton Editar Proceso --------------------------------------------------------------------------------------------

                Process process = new Process();
                process.processCode = processCode;
                process.processName = processName;
                process.fingerprintQuantity = fingerprintQuantity;

                bool result = oBLProcess.Edit(process);
                return Json(new { result = Enums.eCodeError.OK, message = "Actualizado Satisfactoriamente" });

            }
            
        }

        public ActionResult Delete(string id)
        {
            Process process = new Process();
            process.processCode = id;

            bool result = oBLProcess.Delete(process);

            return Json(new { result = Enums.eCodeError.OK, message = "Eliminado Satisfactoriamente" });
        }

        public ActionResult Nuevo()
        {
            UserProcessModel userProcessModel = new UserProcessModel();
            userProcessModel.processCode = "";
            userProcessModel.applicationBussinessCode = "";
            userProcessModel.processBussinessCode = "";
            userProcessModel.processName = "";
            return View("MantProcess",userProcessModel);
        }

        public ActionResult Edit(string processCode)
        {                        
            ProcessList item = oBLProcess.Get(processCode);

            UserProcessModel userProcessModel = new UserProcessModel();
            userProcessModel.processId = item.processId;
            userProcessModel.processCode = processCode;
            userProcessModel.processBussinessCode = item.processBussinessCode;
            userProcessModel.processName = item.processName;
            userProcessModel.processDescription = item.processDescription;
            userProcessModel.applicationId = item.applicationId;
            userProcessModel.applicationBussinessCode = item.applicationBussinessCode;
            userProcessModel.applicationName = item.applicationName;
            userProcessModel.processFingerprintQuantity = item.fingerprintQuantity;

            return View("MantProcess", userProcessModel);
        }



    }
}
