﻿using BAZ.GEN.Log4Dan;
using COIN.BIOM.Data;
using COIN.BIOM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace COIN.BIOM.Service.Controllers
{
    [RoutePrefix("api/enroll")]
    public class EnrollController : ApiController
    {

        private DLEnroll oDLEnroll;
        private Log Log = new Log(typeof(EnrollController));

        public EnrollController()
        {
            oDLEnroll = new DLEnroll();
        }

        [HttpPost]
        [Route("save")]
        public IHttpActionResult save(Enroll model)
        {
            try
            {
                
                bool resul = oDLEnroll.Save(model);
                return Ok(resul);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                throw ex;
            }
        }

        [HttpGet]
        [Route("getEnrolls")]
        public IHttpActionResult getEnrolls(Enroll model)
        {
            try
            {

                List<Enroll> enrolls = oDLEnroll.GetEnrolls();

                return Ok(enrolls);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                throw ex;
            }
        }

    }
}
