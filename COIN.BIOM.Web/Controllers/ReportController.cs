﻿using COIN.BIOM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace COIN.BIOM.Web.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult ReportA()
        {
            return View();
        }


        public ActionResult GetGraficoTipoInc()
        {
            //List<Indicador> lstIndicador = BLIndicador.GetIndicadorTipoInc(CustomerID);
            List<Indicador> lstIndicador = new List<Indicador>();
            lstIndicador.Add(new Indicador { CntIncidencias = 10, TipoIncidencia = "INCIDENCIA 1" });
            lstIndicador.Add(new Indicador { CntIncidencias = 15, TipoIncidencia = "INCIDENCIA 2" });
            lstIndicador.Add(new Indicador { CntIncidencias = 18, TipoIncidencia = "INCIDENCIA 3" });


            return Json(new { result = Enums.eCodeError.OK, data = lstIndicador }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGraficoCriticidad()
        {
            //List<Indicador> lstIndicador = BLIndicador.GetIndicadorCriticidad(CustomerID);

            List<Indicador> lstIndicador = new List<Indicador>();
            lstIndicador.Add(new Indicador { CntIncidencias = 10, Criticidad = "ALTO" });
            lstIndicador.Add(new Indicador { CntIncidencias = 15, Criticidad = "MEDIO" });
            lstIndicador.Add(new Indicador { CntIncidencias = 18, Criticidad = "BAJO" });


            return Json(new { result = Entity.Enums.eCodeError.OK, data = lstIndicador }, JsonRequestBehavior.AllowGet);
        }
    }
}