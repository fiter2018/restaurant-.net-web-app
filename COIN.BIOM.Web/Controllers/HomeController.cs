﻿using COIN.BIOM.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using COIN.BIOM.Business;
using COIN.BIOM.Entity;

namespace COIN.BIOM.Web.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private BLAcceso oBLAcceso;
        private BLUser oBLUser;

        public HomeController()
        {
            oBLAcceso = new BLAcceso();
            oBLUser = new BLUser();
        }

        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }


        [AllowAnonymous]
        public ActionResult Login(LoginModel model)
        {
            try
            {
                Login datos = new Login();
                datos.usuario = model.UserName;
                datos.clave = model.Password;

                int resul = oBLAcceso.ValidarAcceso(datos);

                if (resul == Constants.ErrorUsuarioYOPasswordIncorrectos)
                {
                    ModelState.AddModelError("LogueoForm", "No Coincide el Usuario con el Password.");
                    return View("Index");
                }
                else
                {
                    User user = oBLUser.GetUserByName(model.UserName);

                    if (user.profileId != (int)Enums.eProfile.MOZO)
                    {
                        FormsAuthentication.SetAuthCookie(user.userCode, true);
                        return RedirectToAction("Principal");
                    }
                    else
                    {
                        ModelState.AddModelError("LogueoForm", "El usuario no tiene acceso a este sistema.");
                        return View("Index");
                    }

                }
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("LogueoForm", "Error al Ingresar.");
                return View("Index");
            }

        }
        
        [AllowAnonymous]
        public ActionResult RecuperarClave(LoginModel model)
        {
            Login datos = new Login();
            datos.email = model.Email;

            SalidaWeb resul = oBLAcceso.RecuperarClave(datos);

            if (resul.code == 1)
            {
                return Json(new { result = Enums.eCodeError.ERROR, data = resul });
            }
            else
            {
                return Json(new { result = Enums.eCodeError.OK, data = resul });
            }
        }
        
        [AllowAnonymous]
        public ActionResult VerificarToken(LoginModel model)
        {
            Login datos = new Login();
            datos.recoveryCode = model.recoveryCode;
            datos.token = model.token;

            SalidaWeb resul = oBLAcceso.VerificarToken(datos);

            if (resul.code == 1)
            {
                return Json(new { result = Enums.eCodeError.ERROR, data = resul });
            }
            else
            {
                return Json(new { result = Enums.eCodeError.OK, data = resul });
            }
        }

        [AllowAnonymous]
        public ActionResult Reestablecer(LoginModel model)
        {
            Login datos = new Login();
            datos.nuevaClave = model.nuevaClave;
            datos.confirmaClave = model.confirmaClave;
            datos.token = model.token;

            SalidaWeb resul = oBLAcceso.ReestablecerClave(datos);

            if (resul.code == 1)
            {
                return Json(new { result = Enums.eCodeError.ERROR, data = resul });
            }
            else
            {
                return Json(new { result = Enums.eCodeError.OK, data = resul });
            }
        }

        public ActionResult Principal()
        {
            return View();
        }


        public ActionResult Exit()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }


    }


}