﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using System.Web;
using System.IO;
using BAZ.GEN.Log4Dan;
using COIN.BIOM.Entity;
using COIN.BIOM.Entity.query;
using COIN.BIOM.Business;
using COIN.BIOM.Web.Models;
using WebMatrix.WebData;


namespace COIN.BIOM.Web.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private Log Log = new Log(typeof(UserController));
        private BLUser oBLUser;
        private BLProfile oBLProfile;

        public UserController()
        {
            oBLUser = new BLUser();
            oBLProfile = new BLProfile();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetUserProfiles()
        {
            List<Profile> profiles = oBLProfile.GetProfile();

            return Json(new { result = Enums.eCodeError.OK, data = profiles }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ListUser(string CodUsuario, string NombUsuario, int Dni, int Perfil)
        {            
                        
            List<UserQuery> users = oBLUser.GetUserByParams(CodUsuario, NombUsuario, Dni, Perfil);

            return Json(new { result = Enums.eCodeError.OK, data = users }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DownloadListUser(string CodUsuario = "", string NombUsuario = "", int Dni = 0, int Perfil = 0)
        {
            try
            {
                string FileName = string.Format("ReportListUser_{0:ddMMyyyy_hhmm}.xlsx", DateTime.Now);

                string RutaPlantilla = Server.MapPath("~/Templates/TemplateListUser.xlsx");
                string RutaGuardarComo = Server.MapPath("~/Temporal/" + FileName);

                using (var pck = new OfficeOpenXml.ExcelPackage())
                {
                    using (var stream = System.IO.File.OpenRead(RutaPlantilla))
                    {
                        pck.Load(stream);
                    }

                    List<UserQuery> users = oBLUser.GetUserByParams(CodUsuario, NombUsuario, Dni, Perfil);

                    var ws = pck.Workbook.Worksheets["Data"];
                    int Fila = 2;
                    int Contador = 1;
                    foreach (UserQuery itm in users)
                    {
                        ws.Cells[Fila, 1].Value = Contador;
                        ws.Cells[Fila, 2].Value = itm.userBussinessCode;
                        ws.Cells[Fila, 3].Value = itm.name;
                        ws.Cells[Fila, 4].Value = itm.lastName;
                        ws.Cells[Fila, 5].Value = itm.userName;
                        ws.Cells[Fila, 6].Value = itm.dni;
                        ws.Cells[Fila, 7].Value = itm.profileName;
                        ws.Cells[Fila, 8].Value = itm.email;
                        ws.Cells[Fila, 9].Value = itm.state == 1 ? "ACTIVO" : "INACTIVO";
                        ws.Cells[Fila, 10].Value = string.Format("{0:dd/MM/yyyy HH:mm:ss}", itm.createdDate);
                        Fila++;
                        Contador++;
                    }

                    pck.SaveAs(new FileInfo(RutaGuardarComo));
                }

                return File(RutaGuardarComo, System.Net.Mime.MediaTypeNames.Application.Octet, FileName);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                return Content("No hay registros");
            }
        }

        public ActionResult Save(UserModel model)
        {

            User user = model.user;
            if (user.profileId == 3)
            {
                user.userName = user.userBussinessCode;
                user.password = user.userBussinessCode;
                if (string.IsNullOrEmpty(user.email))
                {
                    user.email = user.userBussinessCode;
                }
            }

            User currentUserLogued = oBLUser.Get(WebSecurity.CurrentUserName);
            user.emisorId = currentUserLogued.emisorId;

            if (string.IsNullOrEmpty(user.userCode)) /*Creación de Usuario*/
            {

                int resul = oBLUser.doValidateUserExistsCreate(user);

                if (resul == (int)Enums.eCodeErrorCreateUser.USERBUSSINESSCODE)
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Código" });
                }
                if (resul == (int)Enums.eCodeErrorCreateUser.USERNAME)
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Username" });
                }
                if (resul == (int)Enums.eCodeErrorCreateUser.DNI)
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Dni" });
                }
                if (resul == (int)Enums.eCodeErrorCreateUser.EMAIL)
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Email" });
                }
                
            } else {  /*Edición de Usuario*/

                int resul2 = oBLUser.doValidateUserExistsEdit(user);

                if (resul2 == (int)Enums.eCodeErrorCreateUser.USERBUSSINESSCODE)
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Código" });
                }
                if (resul2 == (int)Enums.eCodeErrorCreateUser.USERNAME)
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Username" });
                }
                if (resul2 == (int)Enums.eCodeErrorCreateUser.DNI)
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Dni" });
                }
                if (resul2 == (int)Enums.eCodeErrorCreateUser.EMAIL)
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Email" });
                }

            }

            if(user.profileId != 3) { 
                string newPassword = model.user.password;
                string confirmPassword = model.user.confirmPassword;

                if (newPassword != confirmPassword)
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "Las contraseñas no coinciden" });
                }
            }

            bool result = oBLUser.Save(user);

            return Json(new { result = Enums.eCodeError.OK, message = "Guardado Satisfactoriamente" });
            
        }

        public ActionResult Get(string id)
        {
            User item = oBLUser.Get(id);

            return Json(new { result = Enums.eCodeError.OK, data = item }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(string id)
        {
            User user = new User();
            user.userCode = id;

            bool result = oBLUser.Delete(user);

            return Json(new { result = Enums.eCodeError.OK, message = "Eliminado Satisfactoriamente" });
        }

        public ActionResult Locked(string id, int isLocked)
        {
            User user = new User();
            user.userCode = id;
            user.flagLocked = isLocked;

            bool result = oBLUser.Locked(user);

            return Json(new { result = Enums.eCodeError.OK, message = "Bloqueado Satisfactoriamente" });
        }

        public ActionResult Reset(UserModel model)
        {
            User user = model.user;
            string newPassword = model.user.newPassword;
            string confirmPassword = model.user.confirmPassword;

            if (newPassword != confirmPassword)
            {
                return Json(new { result = Enums.eCodeError.ERROR, message = "Las contraseñas no coinciden" });
            }
            else
            {
                bool result = oBLUser.Reset(user);

                return Json(new { result = Enums.eCodeError.OK, message = "Reseteado Satisfactoriamente" });
            }


        }
    }
}
