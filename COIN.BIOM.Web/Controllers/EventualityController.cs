﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using COIN.BIOM.Entity.query;
using COIN.BIOM.Business;
using COIN.BIOM.Web.Models;
using System.Globalization;
using System.IO;
using BAZ.GEN.Log4Dan;

namespace COIN.BIOM.Web.Controllers
{
    [Authorize]
    public class EventualityController : Controller
    {
        private Log Log = new Log(typeof(EventualityController));
        private BLEventuality oBLEventuality;

        public EventualityController()
        {
            oBLEventuality = new BLEventuality();
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DownloadListEventuality(String date1 = "", String date2 = "", int processFilter = 0, int applicationFilter = 0)
        {
            DateTime? CDate1 = null;
            DateTime? CDate2 = null;

            try
            {
                string FileName = string.Format("ReportListEventualities_{0:ddMMyyyy_hhmm}.xlsx", DateTime.Now);

                string RutaPlantilla = Server.MapPath("~/Templates/TemplateListEventuality.xlsx");
                string RutaGuardarComo = Server.MapPath("~/Temporal/" + FileName);

                CDate1 = DateTime.ParseExact(date1, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                CDate2 = DateTime.ParseExact(date2, "dd/MM/yyyy", CultureInfo.InvariantCulture);

                using (var pck = new OfficeOpenXml.ExcelPackage())
                {
                    using (var stream = System.IO.File.OpenRead(RutaPlantilla))
                    {
                        pck.Load(stream);
                    }

                    List<EventlogQuery> lista = oBLEventuality.GetEventlogsByParams(CDate1, CDate2, processFilter, applicationFilter);

                    var ws = pck.Workbook.Worksheets["Data"];
                    int Fila = 2;
                    int Contador = 1;
                    foreach (EventlogQuery itm in lista)
                    {
                        ws.Cells[Fila, 1].Value = Contador;
                        ws.Cells[Fila, 2].Value = itm.userBussinessCode;
                        ws.Cells[Fila, 3].Value = itm.userName;
                        ws.Cells[Fila, 4].Value = itm.completeName;
                        ws.Cells[Fila, 5].Value = itm.profileName;
                        ws.Cells[Fila, 6].Value = itm.dni;
                        ws.Cells[Fila, 7].Value = itm.email;
                        ws.Cells[Fila, 8].Value = itm.userState;
                        ws.Cells[Fila, 9].Value = itm.applicationBussinessCode;
                        ws.Cells[Fila, 10].Value = itm.applicationName;
                        ws.Cells[Fila, 11].Value = itm.processBussinessCode;
                        ws.Cells[Fila, 12].Value = itm.processName;
                        ws.Cells[Fila, 13].Value = string.Format("{0:dd/MM/yyyy HH:mm:ss}", itm.eventlogDatatime);                        
                        ws.Cells[Fila, 14].Value = itm.eventlogResult;
                        Fila++;
                        Contador++;
                    }

                    pck.SaveAs(new FileInfo(RutaGuardarComo));
                }

                return File(RutaGuardarComo, System.Net.Mime.MediaTypeNames.Application.Octet, FileName);
            }
            catch (Exception ex)
            {
                Log.WriteLogError(ex.Message);
                return Content("No hay registros");
            }
        }





    }
}
