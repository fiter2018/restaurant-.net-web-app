﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;
using COIN.BIOM.Web.Models;
using COIN.BIOM.Entity;
using COIN.BIOM.Business;

namespace COIN.BIOM.Web.Controllers
{
    [Authorize]
    public class UserProcessController : Controller
    {

        public ActionResult AddNewUserAndAssignToProcess(UserProcessModel model)
        {

            User user = new User();
            user.userCode = model.userCode;
            user.userBussinessCode = model.userBussinessCode;
            user.userName = model.userName;
            user.password = model.password;
            user.confirmPassword = model.confirmPassword;
            user.name = model.name;
            user.lastName = model.lastName;
            user.email = model.email;
            user.dni = model.dni;
            user.profileId = model.profileId;

            if ((user.profileId == 2) || (user.profileId == 3))
            {
                user.userName = user.userBussinessCode;
                user.password = user.userBussinessCode;
                user.confirmPassword = user.userBussinessCode;
                if (string.IsNullOrEmpty(user.email))
                {
                    user.email = user.userBussinessCode;
                }
            }

            if (user.profileId == 1)
            {
                if (user.password != user.confirmPassword)
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "Las contraseñas no coinciden" });
                }
            }
            
            BLUser oBLUser = new BLUser();
            int resul = oBLUser.doValidateUserExistsCreate(user);

            if (resul == (int)Enums.eCodeErrorCreateUser.USERBUSSINESSCODE)
            {
                return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Código" });
            }
            if (resul == (int)Enums.eCodeErrorCreateUser.USERNAME)
            {
                return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Username" });
            }
            if (resul == (int)Enums.eCodeErrorCreateUser.DNI)
            {
                return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Dni" });
            }
            if (resul == (int)Enums.eCodeErrorCreateUser.EMAIL)
            {
                return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Email" });
            }

            bool resultSaveUser = oBLUser.Save(user);
            User userWithId = oBLUser.GetUserByUserBussinessCode(user.userBussinessCode);

            BLProcess oBLProcess = new BLProcess();
            ProcessList processWithId = oBLProcess.Get(model.processCode);
            
            UserProcess userProcess = new UserProcess();
            userProcess.userId = userWithId.userId;
            userProcess.processId = processWithId.processId;

            BLUserProcess oBLUserProcess = new BLUserProcess();
            bool resultSaveUserProcess = oBLUserProcess.Save(userProcess);

            return Json(new { result = Enums.eCodeError.OK, message = "Nuevo usuario asignado satisfactoriamente" });
            
        }


        public ActionResult assignUserToProcess(string processCode, string processBussinessCode, string userBussinessCode)
        {

            User user = new User();
            user.userBussinessCode = userBussinessCode;
            user.userName = "";
            user.dni = 0;
            user.email = "";

            BLUser oBLUser = new BLUser();
            int result = oBLUser.doValidateUserExistsCreate(user);
            
            if (result == (int)Enums.eCodeErrorCreateUser.ALLOK)
            {
                return Json(new { result = Enums.eCodeError.ERROR, message = "No existe usuario registrado con código ingresado" });
            }
            else
            {            
                User user2 = oBLUser.GetUserByUserBussinessCodeAllStates(userBussinessCode);
                if (user2.state == 2) {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "El usuario ingresado no se encuentra activo" });
                }

                BLProcess oBLProcess = new BLProcess();
                ProcessList processWithId = oBLProcess.Get(processCode);

                UserProcess userProcess = new UserProcess();
                userProcess.userId = user2.userId;
                userProcess.processId = processWithId.processId;

                BLUserProcess oBLUserProcess = new BLUserProcess();
                if (oBLUserProcess.doValidateUserProcessExists(userProcess)) {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "El usuario ya se encuentra asignado al proceso" });
                }

                bool result2 = oBLUserProcess.Save(userProcess);

                return Json(new { result = Enums.eCodeError.OK, message = "Usuario asignado satisfactoriamente" });
            
            }
                        
        }


        /*
        public ActionResult Save(UserProcessModel model)
        {

            User user = model.user;

            if (string.IsNullOrEmpty(user.userCode))
            {
                if (oBLUser.doValidateUserExists(user))
                {
                    return Json(new { result = Enums.eCodeError.ERROR, message = "Ya se registro un usuario con el mismo Login (Username)" });
                }
            }

            string newPassword = model.user.password;
            string confirmPassword = model.user.confirmPassword;

            if (newPassword != confirmPassword)
            {
                return Json(new { result = Enums.eCodeError.ERROR, message = "Las contraseñas no coinciden" });
            }
            else
            {
                bool result = oBLUser.Save(user);

                return Json(new { result = Enums.eCodeError.OK, message = "Guardado Satisfactoriamente" });
            }
        }
        *
        */

        public ActionResult Delete(string userCode, string processCode)
        {
            BLUser oBLUser = new Business.BLUser();
            BLProcess oBLProcess = new BLProcess();
            BLUserProcess oBLUserProcess = new BLUserProcess();

            User user = oBLUser.Get(userCode);
            ProcessList process = oBLProcess.Get(processCode);
            
            UserProcess userProcess = new UserProcess();
            userProcess.userId = user.userId;
            userProcess.processId = process.processId;

            bool result = oBLUserProcess.Delete(userProcess);

            return Json(new { result = Enums.eCodeError.OK, message = "Desvinculado Satisfactoriamente" });
        }
















    }
}
