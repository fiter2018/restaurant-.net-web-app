﻿
$(function () {

    $("#frmBusqueda").on("submit", function (e) {
        e.preventDefault();
    });

    $("#tabPrincipal").DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        columns: [
        { data: null },
        { data: "userCode" },
        { data: "processCode" },
        { data: "userBussinessCode" },
        { data: "completeNameUser" },
        { data: "dni" },
        { data: "processName" },
        ],
        columnDefs: [
            {
                "targets": [1,2],
                "visible": false
            },
            {
                "targets": 0,
                "orderable": false,
                "data": "processCode",
                "render": function (data, type, full, meta) {
                    return "<a href='javascript:fnDesvincular(\"" + data.userCode + "\",\"" + data.processCode + "\");' title='Desvincular' /><i class='fa fa-fw fa-remove'></i></a>";
                }
            }]
    });

    $("#btnGuardar").on("click", function (e) {
        fnGuardar();
    });

    $("#btnAsignarUsuario").on("click", function (e) {
        fnAsignarUsuario();
    });

    $("#frmPrincipal").on("submit", function (e) {
        e.preventDefault();
    });

    fnRefrescar();

});


function fnRefrescar() {
    var params = new Object();
    var processCode = $("#hdnProcessCode").val().length === 0 ? "" : $("#hdnProcessCode").val();
    
    if (processCode === "") {
        //Viene de boton Nuevo
        params.code = "";
        params.name = "";
        params.applicationId = 0;
        var addUserBlock = document.getElementById("addUserBlock");
        addUserBlock.style.display = "none";

    } else {
        //Viene de botón editar proceso
        params.code = $("#txtProcessBussinessCode").val();
        params.name = "";
        params.applicationId = 0;
        $("#txtApplicationBussinessCode").attr("disabled", "disabled");
        $("#txtProcessBussinessCode").attr("disabled", "disabled");

    }

    Get("Process/ListProcessUsersValids", params).done(function (response) {
        if (response.result == 0) {
            fnLimpiarTabla($('#tabPrincipal').dataTable());
            if (response.data.length > 0) {
                $('#tabPrincipal').dataTable().fnAddData(response.data);
            }
        }
    });

}

function fnGuardar() {

    var params = $("#frmBusqueda").serialize();

    $("#frmBusqueda").validate();

    if ($("#frmBusqueda").valid()) {
        Post("Process/Save", params).done(function (response) {
            if (response.result == 0) {
                fnAlertar("Información", response.message, function () {
                    var processCode = $("#hdnProcessCode").val().length === 0 ? "" : $("#hdnProcessCode").val();
                    if (processCode === "") { window.location = fnBaseUrlWeb("Process/Index"); }
                });
            } else {
                fnAlertar("Alerta", response.message);
            }
        });
    }

}

//id == processCode
function fnDesvincular(userCode,processCode) {
    var params = new Object();
    params.userCode = userCode;
    params.processCode = processCode;

    fnConfirmar("Desvincular", "¿Seguro que desea desvincular?", function () {
        Post("UserProcess/Delete", params).done(function (response) {
            if (response.result == 0) {
                fnRefrescar();
            }
        });
    });
}

function fnAsignarUsuario() {
    var params = new Object();
    var processCode = $("#hdnProcessCode").val().length === 0 ? "" : $("#hdnProcessCode").val();    
    params.processCode = processCode;
    params.processBussinessCode = $("#txtProcessBussinessCode").val();
    params.userBussinessCode = $("#txtUserBussinessCode").val();

    if (params.userBussinessCode.length < 4) {
        fnAlertar("Información", "Código inválido, ingrese un código válido.", function () { });
    } else {
        Post("UserProcess/assignUserToProcess", params).done(function (response) {
            if (response.result == 0) {
                fnRefrescar();
            } else {
                fnAlertar("Alerta", response.message);
            }
        });

    }
}


function fnLimpiarControles() {
    $("#txtUserNameMantUser").val("");
    $("#txtPasswordMantUser").val("");
    $("#txtConfirmPasswordMantUser").val("");
    $("#txtNameMantUser").val("");
    $("#txtLastNameMantUser").val("");
    $("#txtEmailMantUser").val("");
    $("#txtDniMantUser").val("");
    $("#ddlPerfilMantUser").val("");

}
