﻿var flagEnviar = 0;
var showed;

$(function () {

    $("#frmBusqueda").on("submit", function (e) {
        if (flagEnviar) {
            flagEnviar = 0;
        } else {
            e.preventDefault();
        }
    });

    $("#tabPrincipal").DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        columns: [
        { data: null },
        { data: null },
        { data: null },
        { data: "processCode" },
        { data: "processBussinessCode" },
        { data: "processName" },
        { data: "applicationName" },
        ],
        columnDefs: [
            {
                "targets": [3],
                "visible": false
            },
            {
                "targets": 0,
                "orderable": false,
                "data": "processCode",
                "render": function (data, type, full, meta) {
                    return "<a href='" + fnBaseUrlWeb("Process/Edit?processCode=" + data.processCode) + "' title='Editar' /><i class='fa fa-fw fa-edit'></i></a>";
                }
            },
            {
                "targets": 1,
                "orderable": false,
                "data": "processCode",
                "render": function (data, type, full, meta) {
                    return "<a href='javascript:fnEliminar(\"" + data.processCode + "\");' title='Eliminar' /><i class='fa fa-fw fa-remove'></i></a>";
                }
            },
            {
                "targets": 2,
                "orderable": false,
                "data": "processCode",
                "render": function (data, type, full, meta) {
                    return "<a href='javascript:fnAgregarUsuario(\"" + data.processCode + "\");' title='Agregar Usuario' /><i class='fa fa-fw fa-user-plus'></i></a>";
                }
            }]
    });


    $("#btnBuscar").on("click", function (e) {
        fnRefrescar();
    });

    
    $("#btnNuevo").on("click", function (e) {
        //fnLimpiarControles();
        //$("#divPrincipal").modal("show");

    });

    $("#btnExportar").on("click", function (e) {
        flagEnviar = 1;
        $("#frmBusqueda").submit();
        //fnExportar();
    });
    
    $("#btnGuardar").on("click", function (e) {
        fnGuardar();
    });

    $("#frmPrincipal").on("submit", function (e) {
        e.preventDefault();
    });

    document.getElementById("ddlPerfilMantUserInProcess").addEventListener("change", fnOnChangeSelectMantUserInProcess);

    fnRefrescar();

    $("#frmPrincipal").validate({
        rules: {
            "userBussinessCode": {
                required: true
            },
            "email": {
                email: true
            },
            "dni": {
                required: true,
                minlength: 8
            },
            "userName": {
                required: true,
                minlength: 6
            },
            "name": {
                required: true
            },
            "lastName": {
                required: true
            },
            "password": {
                required: true,
                minlength: 6
            },
            "confirmPassword": {
                required: true,
                minlength: 6,
                equalTo: "#txtPasswordMantUser"
            },
        }

    });

});

function fnOnChangeSelectMantUserInProcess() {
    console.log("case " + document.getElementById("ddlPerfilMantUserInProcess").value + " onchange event executed");
    switch (document.getElementById("ddlPerfilMantUserInProcess").value) {
        case "1": if (showed === true) { } else { fnToggleShowHideInputs(); } break;
        case "2": case "3": if (showed === true) { fnToggleShowHideInputs(); } else { } break;
        default: if (showed === true) { } else { fnToggleShowHideInputs(); } break;
    }
}

function fnToggleShowHideInputs() {

    var userNameElem = document.getElementById("txtUserNameMantUserInProcess");
    var passElem = document.getElementById("txtPasswordMantUserInProcess");
    var confirmPassElem = document.getElementById("txtConfirmPasswordMantUserInProcess");
    var emailElem = document.getElementById("txtEmailMantUserInProcess");

    if (document.getElementById("divUsernameMantUserInProcess").style.display === "none") {
        document.getElementById("divUsernameMantUserInProcess").style.display = "block";
        document.getElementById("divPassMantUserInProcess").style.display = "block";
        document.getElementById("divConfirmPassMantUserInProcess").style.display = "block";

        userNameElem.setAttribute("required", "required");
        passElem.setAttribute("required", "required");
        confirmPassElem.setAttribute("required", "required");
        //emailElem.setAttribute("required", "required");
        $("#txtEmailMantUserInProcess").prop("required", true);

        userNameElem.value = "";
        passElem.value = "";
        confirmPassElem.value = "";
        showed = true;
        console.log("mostrado");
    } else {
        document.getElementById("divUsernameMantUserInProcess").style.display = "none";
        document.getElementById("divPassMantUserInProcess").style.display = "none";
        document.getElementById("divConfirmPassMantUserInProcess").style.display = "none";

        userNameElem.removeAttribute("required");
        passElem.removeAttribute("required");
        confirmPassElem.removeAttribute("required");
        //emailElem.removeAttribute("required");
        $("#txtEmailMantUserInProcess").prop("required", false);

        userNameElem.value = " ";
        passElem.value = " ";
        confirmPassElem.value = " ";
        showed = false;
        console.log("ocultado");
    }

}


function fnRefrescar() {
    var params = new Object();
    params.code = $("#txtProcessBussinessCode").val().length === 0 ? "" : $("#txtProcessBussinessCode").val();
    params.name = $("#txtProcessName").val().length === 0 ? "" : $("#txtProcessName").val();

    if ($("#ddlAplicacion").val() === "" || $("#ddlAplicacion").val() === null) {
        params.applicationId = 0;
    } else {
        var aplicacionInt = parseInt($("#ddlAplicacion").val());
        if (isNaN(aplicacionInt)) {
            params.applicationId = 0;
        } else {
            params.applicationId = aplicacionInt;
        }
    }

    Get("Process/List", params).done(function (response) {
        if (response.result == 0) {
            fnLimpiarTabla($('#tabPrincipal').dataTable());
            if (response.data.length > 0) {
                $('#tabPrincipal').dataTable().fnAddData(response.data);
            }
        }
    });
}

function fnExportar() {
    var params = new Object();
    params.code = $("#txtProcessBussinessCode").val().length === 0 ? "" : $("#txtProcessBussinessCode").val();
    params.name = $("#txtProcessName").val().length === 0 ? "" : $("#txtProcessName").val();

    if ($("#ddlAplicacion").val() === "" || $("#ddlAplicacion").val() === null) {
        params.applicationId = 0;
    } else {
        var aplicacionInt = parseInt($("#ddlAplicacion").val());
        if (isNaN(aplicacionInt)) {
            params.applicationId = 0;
        } else {
            params.applicationId = aplicacionInt;
        }
    }

    Get("Process/DownloadListProcessUsers", params).done(function (response) {

        fnAlertar("Información", "Documento exportado en carpeta 'Temporal'", function () {
            fnRefrescar();
        });

    });
}

function fnGuardar() {

    //Guardando usuario en tabla usuario
    var params = $("#frmPrincipal").serialize();

    /*
    if (showed === true) {
        console.log("btn Guardar showed true");
        $("#frmPrincipal").validate({
            rules: {
                "user.userBussinessCode": {
                    required: true
                },
                "email": {
                    required: true,
                    email: true
                },
                "user.dni": {
                    required: true,
                    minlength: 8
                },
                "user.userName": {
                    required: true,
                    minlength: 6
                },
                "user.name": {
                    required: true
                },
                "user.lastName": {
                    required: true
                },
                "user.password": {
                    required: true,
                    minlength: 6
                },
                "user.confirmPassword": {
                    required: true,
                    minlength: 6,
                    equalTo: "#txtPasswordMantUser"
                },
            }
        });
    }
    else
    {
        console.log("btn Guardar showed false");
        $("#frmPrincipal").validate({
            rules: {
                "user.userBussinessCode": {
                    required: true
                },
                "email": {
                    email: true
                },
                "user.dni": {
                    required: true,
                    minlength: 8
                },
                "user.name": {
                    required: true
                },
                "user.lastName": {
                    required: true
                },
            }
        });
    }*/
    
    if ($("#frmPrincipal").valid()) {
        Post("UserProcess/AddNewUserAndAssignToProcess", params).done(function (response) {
            if (response.result == 0) {
                fnAlertar("Información", response.message, function () {
                    $("#divPrincipal").modal("hide");
                });
            } else {
                fnAlertar("Alerta", response.message);
            }
        });
    }
    
    //Guardando usuario-proceso en tabla UserProcess


}


//id == processCode
function fnEditar(id) {

    Get("Process/Nuevo/" + id).done(function (response) {
        if (response.result == 0) {
            $("#hdnCustomerCode").val(id);
            $("#customer_code").val(response.data.code);
            $("#customer_name").val(response.data.name);
            $("#customer_ruc").val(response.data.ruc);

            $("#customer_code").attr("readonly", "readonly");


            $("#divPrincipal").modal("show");
        }
    });
}
//id == processCode
function fnEliminar(id) {
    var params = new Object();
    params.Id = id;

    fnConfirmar("Eliminar", "¿Seguro que desea eliminar?", function () {
        Post("Process/Delete", params).done(function (response) {
            if (response.result == 0) {
                fnRefrescar();
            }
        });
    });
}
//id == processCode
function fnAgregarUsuario(id) {
    fnLimpiarControles();
    showed = true;
    document.getElementById("divUsernameMantUserInProcess").style.display = "block";
    document.getElementById("divPassMantUserInProcess").style.display = "block";
    document.getElementById("divConfirmPassMantUserInProcess").style.display = "block";
    document.getElementById("txtUserNameMantUserInProcess").setAttribute("required", "required");
    document.getElementById("txtPasswordMantUserInProcess").setAttribute("required", "required");
    document.getElementById("txtConfirmPasswordMantUserInProcess").setAttribute("required", "required");
    //document.getElementById("txtEmailMantUserInProcess").setAttribute("required", "required");
    $("#txtEmailMantUserInProcess").prop("required", true);

    $("#hdnProcessCode").val(id);
    $("#hdnUserCode").val("");
    Get("Process/Get/" + id).done(function (response) {
        if (response.result == 0) {
            $("#txtProcessNameMantUserInProcess").val(response.data.processName);
            $("#txtProcessNameMantUserInProcess").attr("readonly", "readonly");
        }
    });
    $("#divPrincipal").modal("show");
}


function fnLimpiarControles() {
    $("#txtUserNameMantUserInProcess").val("");
    $("#txtPasswordMantUserInProcess").val("");
    $("#txtConfirmPasswordMantUserInProcess").val("");
    $("#txtNameMantUserInProcess").val("");
    $("#txtLastNameMantUserInProcess").val("");
    $("#txtEmailMantUserInProcess").val("");
    $("#txtDniMantUserInProcess").val("");
    $("#ddlPerfilMantUserInProcess").val("");

}
