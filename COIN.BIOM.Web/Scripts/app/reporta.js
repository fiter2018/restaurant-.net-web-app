﻿$(function () {

    //$("#ddlClienteBus").on("change", function (e) {
    //    fnRefrescarGrafico();
    //    fnRefrescarGrafico2();
    //});

    fnRefrescarGrafico();
    fnRefrescarGrafico2();
});

function fnRefrescarGrafico() {
    var clienteCli = $("#hdnCodCli").val();
    var perfil = $("#hdnPerfil").val();
    var params = new Object();

    if (perfil == 1) {
        var clienteAdm = $("#ddlClienteBus").val();
        params.CustomerID = clienteAdm;
    }
    else {
        params.CustomerID = clienteCli;
    }

    if (params.CustomerID == "") {
        params.CustomerID = 0
    }

    Get("Report/GetGraficoTipoInc", params).done(function (response) {
        if (response.result == 0) {
            fnCargarGrafico(response.data);
        }
    });
}

function fnRefrescarGrafico2() {
    var clienteCli = $("#hdnCodCli").val();
    var perfil = $("#hdnPerfil").val();
    var params = new Object();

    if (perfil == 1) {
        var clienteAdm = $("#ddlClienteBus").val();
        params.CustomerID = clienteAdm;
    }
    else {
        params.CustomerID = clienteCli;
    }

    if (params.CustomerID == "") {
        params.CustomerID = 0
    }

    Get("Report/GetGraficoCriticidad", params).done(function (response) {
        if (response.result == 0) {
            fnCargarGrafico2(response.data);
        }
    });
}

function fnCargarGrafico(data) {
    var categorias = [];
    var series = [];
    var datosgrafico = [];

    $.each(data, function (itemNo, item) {
        categorias.push(item.TipoIncidencia);
        datosgrafico.push(item.CntIncidencias);
    });

    series.push({
        name: 'Incidencias',
        color: '#2f7ed8',
        data: datosgrafico
    });


    $('#GraficoReporte').html("&nbsp;");

    $('#GraficoReporte').highcharts({
        chart: {
            //type: 'bar'
            type: 'column'
        },

        title: {
            text: 'Incidencias por tipo de incidente'
        },

        xAxis: {
            categories: categorias,
            title: {
                text: null
            }
        },

        yAxis: {
            min: 0,
            max: 30,
            title: {
                text: 'Cantidad',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ''
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        credits: {
            enabled: false
        },
        series: series
    });
}

function fnCargarGrafico2(data) {

    var series = [];
    var dataSeries = [];
    $.each(data, function (index, element) {
        dataSeries.push({ name: element.Criticidad, y: element.CntIncidencias });
    });

    series.push({
        name: "Criticidad",
        colorByPoint: true,
        data: dataSeries
    })

    $('#GraficoReporte2').html("&nbsp;");

    $('#GraficoReporte2').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Incidencias por criticidad'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.2f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.2f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        credits: {
            enabled: false
        },
        series: series
    });
}
