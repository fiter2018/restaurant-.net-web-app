﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity
{
    public class User
    {
        public string userCode { get; set; }
        public string userBussinessCode { get; set; }
        public string profileName { get; set; }
        public byte[] FingerTemplate { get; set; }
        //public string flgPasswordChange { get; set; }

        public int userId { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string lastName { get; set; }
        public int dni { get; set; }
        public int enrollId { get; set; }
        public int emisorId { get; set; }
        public int profileId { get; set; }
        public int createdUser { get; set; }
        public string email { get; set; }

        public string newPassword { get; set; }
        public string confirmPassword { get; set; }

        public int state { get; set; }
        public int flagLocked { get; set; }

        public bool viewNotificationFlag { get; set; }
        public int repeatingTime { get; set; }




    }
}
