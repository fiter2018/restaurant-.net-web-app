﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity
{
    public class Indicador
    {
        public int CntIncidencias { get; set; }
        public string TipoIncidencia { get; set; }
        public string Criticidad { get; set; }

    }
}
