﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity.model
{
    public class EnrollUserModel
    {
        public string tokenCode { get; set; }
        public string processBussinessCode { get; set; }
        public string userBussinessCode { get; set; }
        public string name { get; set; }
        public string lastName { get; set; }
        public string dni { get; set; }
        public byte[] fingerTemplate { get; set; }

    }
}
