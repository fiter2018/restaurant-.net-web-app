﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity.model
{
    public class SessionModel
    {

        public string tokenCode { get; set; }
        public string processBussinessCode { get; set; }
        public string applicationBussinessCode { get; set; }
        public string userBussinessCode { get; set; }

    }
}
