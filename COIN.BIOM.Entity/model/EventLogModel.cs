﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity.model
{
    public class EventLogModel
    {
        public string tokenCode { get; set; }
        public string processBussinessCode { get; set; }
        public string userBussinessCode { get; set; }
        public string eventDateTime { get; set; } //"dd/MM/yyyy HH:mm:ss"
        public int eventResult { get; set; }

    }
}
