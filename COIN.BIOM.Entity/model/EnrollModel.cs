﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity.model
{
    public class EnrollModel
    {
        public string tokenCode { get; set; }
        public string processBussinessCode { get; set; }
        public string userCode { get; set; }
        public byte[] fingerTemplate { get; set; }

    }
}
