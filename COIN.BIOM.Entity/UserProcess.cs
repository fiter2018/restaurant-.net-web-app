﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity
{
    public class UserProcess
    {
        public int userId { get; set; }
        public int processId { get; set; }
        public DateTime? assignmentDate { get; set; }

    }
}
