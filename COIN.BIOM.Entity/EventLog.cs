﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity
{
    public class EventLog
    {
        public int eventId { get; set; }
        public int userId { get; set; }
        public int applicationId { get; set; }
        public int processId { get; set; }
        public DateTime eventDatetime { get; set; }
        public int eventResult { get; set; }

    }
}
