﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity
{
    public class Process
    {
        public int processId { get; set; }
        public string processCode { get; set; }
        public string processBussinessCode { get; set; }
        public string processName { get; set; }
        public string processDescription { get; set; }
        public int applicationId { get; set; }
        public string applicationBussinessCode { get; set; }
        public string applicationName { get; set; }
        public int fingerprintQuantity { get; set; }
        public int state { get; set; }
        public int createdUser { get; set; }
        public DateTime? createdDate { get; set; }

    }

    public class ProcessList
    {
        public int processId { get; set; }
        public string processCode { get; set; }
        public string processBussinessCode { get; set; }
        public string processName { get; set; }
        public string processDescription { get; set; }
        public int applicationId { get; set; }
        public string applicationBussinessCode { get; set; }
        public string applicationName { get; set; }
        public int fingerprintQuantity { get; set; }
        public int state { get; set; }
        public int createdUser { get; set; }
        public DateTime? createdDate { get; set; }

    }

}
