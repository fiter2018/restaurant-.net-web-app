﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity
{
    public class Enums
    {

        public enum eCodeError : int
        {
            OK = 0,
            ERROR = 1,
            VAL = 2
        }

        public enum eStatus : int
        {
            ACTIVE = 1,
            INACTIVE = 2
        }

        public enum eProfile : int
        {
            ADMINISTRADOR = 1,
            DUEÑO = 2,
            MOZO = 3,
            CAJERO = 4,
            COCINERO = 5
            
        }

        public enum eCodeErrorCreateUser : int
        {
            USERBUSSINESSCODE = 1,
            USERNAME = 2,
            DNI = 3,
            EMAIL = 4,
            ALLOK = 5
        }


    }
}
