﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity
{
    public class Sale
    {
        public int emisorId { get; set; }
        public int saleId { get; set; }
        public string paymentType { get; set; }
        public decimal? paymentAmount { get; set; }
        public int customerId { get; set; }
        public DateTime? createdDate { get; set; }
        public int createdUser { get; set; }
        
    }
}
