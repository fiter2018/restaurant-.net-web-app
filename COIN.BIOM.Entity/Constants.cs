﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity
{
    public class Constants
    {
        public const int ErrorUsuarioNoExiste = 1;
        public const int ErrorUsuarioNoActivo = 2;
        public const int ErrorUsuarioMozo = 3;

        public const int ErrorUsuarioYOPasswordIncorrectos = 1;

        public const int TipoInicioSesion = 1;
        public const int TipoRecuperacionClave = 2;

        public const int ErrorCodigoToken = 1;
        public const int ErrorCodigoRecuperacion = 2;
        public const int ErrorVigenciaCodigoRecuperacion = 3;

        public const int ErrorVigenciaToken = 2;
        public const int ErrorNoExisteIdUserXToken = 3;

        public const int TodoOK = 1;   

        public const string TxtTodos = "-- TODOS --";

        public const string AppName = "Copeinca AS";

        public const string AsuntoSoporte = "Soporte Copeinca";

        public const string AsuntoRecuperarClave = "Recuperar Clave";

        public const string AsuntoNuevoIncidente = "Eulen SAFE - Nuevo Incidente";

    }
}
