﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity
{
    public class SalidaWeb
    {
        public int code { get; set; }
        public string message { get; set; }
        public string data { get; set; }
        
    }
}
