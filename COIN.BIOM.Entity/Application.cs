﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity
{
    public class Application
    {
        public int applicationId { get; set; }
        public string applicationBussinessCode { get; set; }
        public string applicationName { get; set; }
        
    }
}
