﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity
{
    public class Login
    {
        public string usuario { get; set; }
        public string email { get; set; }
        public string clave { get; set; }
        public string token { get; set; }
        public string recoveryCode { get; set; }
        public string nuevaClave { get; set; }
        public string confirmaClave { get; set; }
        
    }
}
