﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity.query
{
    public class UserEnrollQuery
    {
        public string userCode { get; set; }
        public string name { get; set; }
        public string lastName { get; set; }
        public string dni { get; set; }
        public List<EnrollQuery> fingersTemplate { get; set; }


        public string fullName
        {
            get
            {
                return string.Format("{0} {1}", name, lastName);
            }
        }

    }

}
