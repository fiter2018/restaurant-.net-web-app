﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity.query
{
    public class UserQuery
    {
        public string userCode { get; set; }
        public string userBussinessCode { get; set; }
        public int flagLocked { get; set; }
        public string name { get; set; }
        public string lastName { get; set; }
        public string userName { get; set; }
        public string profileName { get; set; }
        public int dni { get; set; }
        public int state { get; set; }
        public string email { get; set; }
        public DateTime? createdDate { get; set; }
        

    }

}
