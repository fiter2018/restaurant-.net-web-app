﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity.query
{
    public class EventlogQuery
    {

        public string userBussinessCode { get; set; }
        public string userName { get; set; }
        public string completeName { get; set; }
        public string profileName { get; set; }
        public int dni { get; set; }
        public string email { get; set; }
        public string userState { get; set; }
        public string applicationBussinessCode { get; set; }
        public string applicationName { get; set; }
        public string processBussinessCode { get; set; }
        public string processName { get; set; }
        public DateTime? eventlogDatatime { get; set; }
        public string eventlogResult { get; set; }
        
    }
}
