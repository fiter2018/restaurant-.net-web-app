﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity.query
{
    public class ProcessUsersQuery
    {
        public string processCode { get; set; }
        public string processBussinessCode { get; set; }
        public string processName { get; set; }
        public string processDescription { get; set; }
        public string applicationBussinessCode { get; set; }
        public string applicationName { get; set; }
        public string userCode { get; set; }
        public string userBussinessCode { get; set; }
        public string completeNameUser { get; set; }
        public string userName { get; set; }
        public int dni { get; set; }
        public string profileName { get; set; }
        public string email { get; set; }
        public DateTime? assignmentDate { get; set; }        

    }
}
