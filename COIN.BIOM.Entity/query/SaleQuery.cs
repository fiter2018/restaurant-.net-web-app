﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity.query
{
    public class SaleQuery
    {
        public int emisorId { get; set; }
        public int saleId { get; set; }
        public string paymentType { get; set; }
        public decimal? paymentAmount { get; set; }
        public int customerId { get; set; }
        public DateTime? createdDate { get; set; }
        public int createdUser { get; set; }
        public string completeName { get; set; }
        public int attentionOrderDetailId { get; set; }
        public string productBussinessCode { get; set; }
        public string description { get; set; }
        public decimal? unitValue { get; set; }
        public int quantity { get; set; }
        public decimal? subTotal { get; set; }
        
    }
}
