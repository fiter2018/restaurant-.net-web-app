﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COIN.BIOM.Entity.query
{
    public class EnrollQuery
    {
        public string userCode { get; set; }
        public byte[] fingerTemplate { get; set; }
    }
}
